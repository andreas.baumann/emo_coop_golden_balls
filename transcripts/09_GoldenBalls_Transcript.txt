HOST: Do you want the secret of eternal youth? 
SPEAKER1:I do not believe I have a reason to lie. 
HOST: Do you want money? 
SPEAKER2: I've been proven in this show to be honest, and you haven't. 
HOST: Do you want lies? 
SPEAKER3: You believe him? You will regret it.
HOST: Well, we can give you two of them. Guess which? It's golden balls. 

[CUT]

HOST: Thank you. Thank you. Very kind. Welcome to the show, where you can win a fortune, but only if you've got the balls. Yep, it's golden balls. Once again, we have four players looking to bluff their way to the big money. A tourism manager, a driving instructor, an investigating officer and a fashion student. But who's got what it takes to take the lot? And here's the star of the show. Yes, it's the golden ball. And there's a hundred more just like this one behind me up in the golden bank. They're waiting to come out to play. We want them to come out to play. So let's start the ball machine. Ah, this is the bit. I love watching all that money swirling around up there, because we all know that one of those golden walls is worth an incredible seventy five thousand pounds. Will we see it today? Well, fingers crossed. But in this game, where there's cash, hey, there's killers. And the queen of the killers is Amanda, ooh. And there she is doing what she does best, sticking four of those terrible killer balls into the mix. And so there they are, the first 16 golden balls of today's game, cash and killers. And as always, each player receives four balls at random. That's two for the front row and two for the back. Hi, everybody. Welcome to Golden Ball. Are you ready for this? 
SPEAKER1: We’re ready.
HOST: We have four very interesting people with us today. I'll be fascinated to see how this works out. Now, sixteen golden balls out there. As always, four of them are killers. We don't know where they are. Twelve of them are cash balls. We don't know how much they're worth. Let's find out what's going on. Our first player today is Richard from Derbyshire. Good to see you.

[ROUND 1]

SPEAKER1: Hi there.
HOST: Richard, I think a little bow is in order. You are our first ever baronet on the show. 
SPEAKER1: Oh, I'm glad to be the first. Jasper. Thank you.
HOST: You are indeed. You actually own a sort of a stately home. 
SPEAKER1: That's right.
HOST: But it's haunted. You've got a haunted stately home.
SPEAKER1: Certainly. That's right. And we also do ghost hunting. 
HOST: Yes?
SPEAKER1: Right. That's right. You can come and stop the night from eight in the evening to eight in the morning and ghost hunt. 
HOST: Have you seen any ghosts? 
SPEAKER1: I certainly haven't seen one, Jasper, but I certainly know that there are some in the house. 
HOST: Alarming, and do you get all that stuff? The camera equipment all goes up the chute and nobody knows what's happening.
SPEAKER1: Uhm, that's right. Well, some people bring camcorders into particular rooms and they've charged up their battery full for the event. They get into that room and it's empty.
HOST: Oh, spooky. Richard, you've got two golden balls on the front here. 
SPEAKER1: Mhm.
HOST: We don't know what they are. Let us know what you've got, please.
SPEAKER1: First one is cash pool thirty pounds times. 
HOST: Thirty pounds, not a killer. 
RICHARD: And the second one is cash pool six thousand.
HOST: That’s more like it. Our next player is Ange from Gloucestershire. Hello, Ange. Is it Angela? Angelina? 
SPEAKER2: It's Angela, really. But it’s only when I'm a bad girl. 
HOST: And when your mom goes “Angela?”
SPEAKER2: Yes.
HOST: Where in Gloucester? 
SPEAKER2: Dursley. 
HOST: Dursley. Yeah, all right. You class yourself as a sort of funky gran. 
SPEAKER2: Well, I'd like to say I'm pretty trendy for a granny. 
HOST: So what's funky about you, then? 
SPEAKER2: Well, I've got my tongue pierced. I have a tattoo. Have you? Where? 
HOST: Do you lie? Wooh! 
SPEAKER2: Haha.
HOST: And you've got a new bloke.
SPEAKER2: I have, yes. I do. 
HOST: You found him on the Internet?
SPEAKER2: I did find him on the internet, yeah. 
HOST: And it's all a bit of a whirlwind, isn't it? 
SPEAKER2: Absolutely. Known him a month and… Love it for a sight, Jasper.
HOST: You've been on holiday. He's going to move in. 
SPEAKER2: Yeah, absolutely.
HOST: Where'd you find him? On ebay? 
SPEAKER2: Not bad, though.
HOST: Well, we wish you the best. 
SPEAKER2: Thank you very much, Jasper.
HOST: What have you got on your front row, Ange? 
SPEAKER2: Okay, so on my front row. Oh, I have a killer. 
HOST: Eh, there's four of them.
SPEAKER2: Never mind. My second one is a cash ball, three hundred fifty pounds. 
HOST: All right, one killer on show, three to find. Our next player is Dave from Kent. Ah, some supporters in, David? 
SPEAKER3: Yes, thank you very much. 
HOST: What whereabouts in Kent? 
SPEAKER3: Dilton in Kent.
HOST: And what do you do? 
SPEAKER3: I'm an investigating officer for the police. 
HOST: Oh, you're sort of working the police force or just associated with it? 
SPEAKER3: No, I work for… sorry. Police. I'm a civilian there. 
HOST: You're a civilian? Yeah. But I do know when you have time up, you go on the beat.
SPEAKER3: Yes, I do. 
HOST: And you investigate minor crime like violence, drugs and robbery. What are the major ones? Are they like parking, beating, not paying your congestion charge? 
SPEAKER3: So, yeah.
HOST: Yeah? Before you joined the police, you were a prison officer? 
SPEAKER3: Yes, I was. 
HOST: And you specialized in going through prisoners post and parcels, looking for ladies underwear that their wives and girlfriends sent them? 
SPEAKER3: That has been known before. Yeah. It's quite disturbing. 
HOST: It's a bit pervy, but you've got some lucky underpants. 
SPEAKER3: Yes, I have, Jasper.
HOST: And you're wearing them today? 
SPEAKER3: Certainly am. 
HOST: Okay, let's see if they're working. What have you got in the front row there, Dave? 
SPEAKER3: Well, two hundred.
HOST: Two hundred and? 
SPEAKER3: Sixteen hundred. 
HOST: That's a bit better. One killer on the show, still three to find. I wonder if our last player has any killers on the front row. It's Natalie from Coventry. Hi, Natalie. Lovely to see you. Now then, your last name is? 
SPEAKER4: Chalinsky. 
HOST: Chalinsky? 
SPEAKER4: Yes.
HOST: Now, where does that originate? 
SPEAKER4: Poland.
HOST: Oh, are your parents Polish and no, my granddad was Polish. Because There's a tremendous amount of Poles in England. 
SPEAKER4: Yeah, there is.
HOST: Yeah. And you're a very keen dancer. I know that.
SPEAKER4: I love raving. 
HOST: According to someone that said, you're going to show me some shapes. 
SPEAKER4: Yeah, I think you could, like, do some for me, didn't you? 
HOST: What? 
SPEAKER4: Do some shapes for me.
HOST: Could I? Yeah, I'll do. 
SPEAKER4: Yeah. 
HOST: Richard, help me out. And you're the secretary of the… 
SPEAKER4: Wine Society
HOST: Nottingham? 
SPEAKER4: Yes. 
HOST: What do students know about wine? 
SPEAKER4: We do, honestly. 
HOST: Come on.
SPEAKER4: We have someone come in to do the speaking with us. And I learned, like, wine has some legs. 
HOST: Oh, wine.
SPEAKER4: And I've learned how to smell it and taste it. 
HOST: What goes well with pot noodles? Pizza and beans and kebabs. Okay, Natalie, you're the last person up today. What have you got on your front row? 
SPEAKER4: Three thousand
HOST: And, oh!
SPEAKER4: Eleven thousand. 
HOST: Well, you'll be able to test some wine now if you come under that. That's terrific. Okay, we only have one killer on show, so there's three to five. They could be anywhere. You each have two golden balls on the back. You don't know what they are. We don't, but you're going to have a look, and then I'm going to come back and find out what you claim that you have on the back road. 
SPEAKER1: I'm a great cricket fan, and today on goldenballs, I'm going to play with a straight, honest bat.
SPEAKER2: As a driving instructor, I hope the big red L on my car doesn't stand for loser. 
SPEAKER3: I'm a master at telling if someone's lying, they lie to me today. I will not trust them in the next round.
SPEAKER4: Today on golden balls, I'm going to dance my way into the final and stomp out anyone with killers. 
HOST: You've now all seen what you have on the back row. We have one killer on show, so it's now a killer hunt. Natalie, let's start with you. You've got eleven thousand and three thousand. What have you got on the back? 
SPEAKER4: My backwalls aren't quite as good, but I think that I'm in a strong position, so I can be completely honest with you. I do have a killer ball here, and I have seventy five pounds in this ball. However, I do have 14 grand on the front row, so Ithink that does keep me in a very strong position in this game. 
HOST: So you've got a killer ball 
SPEAKER4: Yeah 
HOST: And seventy five. David, what have you got on the back? 
SPEAKER3: I've got a very nice fifteen thousand pound ball there and a five grand ball there. 
HOST: Ange? I have a fifteen hundred ball there and a ten thousand pound lovely ball there.
HOST: Ten thousand and fifteen hundred. 
SPEAKER2: Yep. 
HOST: Richard, you must have two killers.
SPEAKER1: I've got one killer here. You're absolutely right, Jasper. I've got one here, but I've got a 15,000 pound ball here in the right hand one.
HOST: Okay, Natalie, who's got the missing killer? 
SPEAKER4: I'd like to say, Ange, purely because you do have the weakest front row. 
SPEAKER2: No, you’re totally wrong, Natalie. 
SPEAKER4: I think if you had a killer back then, you wouldn't admit it. 
SPEAKER2: No, I wouldn't admit it.
SPEAKER4: Exactly.
SPEAKER2: I can assure you I've got one killer, and that killer is on the show. 
SPEAKER4: I mean, David. 
SPEAKER2: David hasn't got a huge amount of money yet. 
SPEAKER3: I haven’t got a huge amount of money, but I've got a fifteen grand ball sitting there.
SPEAKER2: You said you have a ten thousand pound ball and a fifteen hundred ball. That killer on my front row is the only killer I have in my possession. 
HOST: What have you got again? 
SPEAKER3: I've got fifteen thousand pound ball, I’ve got five thousand pound ball.
SPEAKER2: So, either somebody's got two killers
HOST: And what balls have you got? 
SPEAKER1: I’ve got a fourteen thousand here.
SPEAKER2: No, fifteen thousand you said. You declared that ball to be fifteen thousand pound ball. 
SPEAKER1: No, fourteen thousand, fourteen thousand pound. 
SPEAKER2: Now you want to tell… fifteen, fifteen thousand pounds
SPEAKER3: Tell me, it’s a killer
HOST: That's what it is, Richard. Just in your own just be very careful. 
SPEAKER1: Yeah, fourteen thousand pound ball. I don’t care about this side
SPEAKER3: I Completely. I don't believe you at all. 
SPEAKER1: You've got the fifteen thousand.
SPEAKER3: Yeah
SPEAKER1: And I've got the fourteen thousand pound ball. 
SPEAKER4: You did say you have fifteen. 
SPEAKER1: I'm very sorry. After I'd heard that you'd said fifteen thousand. Fourteen thousand here and killer ball. And a killer ball here. What you got, Natalie, over? 
SPEAKER4: I’ve got a killer. 
SPEAKER1: And seventy five over there. I personally think that Richard's got the other killer
SPEAKER2: And I do, because that's why I made it such a huge amount of money.
SPEAKER1: Now I've got a fourteen thousand pound ball and a killer ball. Dave, I'm not so sure about that fifteen thousand that you’ve said 
SPEAKER3: If you throw me out. It's ridiculous, because I can show it proved you that's a 15 grand ball. 
SPEAKER4: Be honest. You've got, like, reasonable front row. 
SPEAKER3: Exactly.
SPEAKER4: So I don't believe that you've got a reason to lie. If I had that front row, it wouldn't give me… 
SPEAKER1: I’ve got a very good front row. I’ve got over 20,000 pounds in here to take through there.
SPEAKER4: You’ve got a good row but you have one killer at the back and admitted that.
SPEAKER3: It is a good amount of money take through, but that's not a 14 grand ball, that's the gap for killer.
SPEAKER1: 14 grand ball 
SPEAKER3: That’s not
SPEAKER1: And that's a killer ball. 
SPEAKER3: I don't believe you. 
HOST: As I said, it's about trust and belief. You have to sort this out, you know. 
SPEAKER3: Sorry, Richard. What have you got? 
SPEAKER1: Fourteen thousand pounds and a killer.
SPEAKER3: You definitely haven't got another killer
SPEAKER1: I’ve got over £20,000 in my hand here to take through to the next round. I think that's a pretty good hand.
HOST: Where do you think the killer is, Richard? 
SPEAKER1: I think it's over the other side of the table here. I'm not so sure about your left hand ball, Dave. You said…
SPEAKER3: That's five thousand pounds. I guarantee you that. 
SPEAKER1: Natalie, have you got another killer? 
SPEAKER4: No, I've got one that’s got declared.
SPEAKER3: Be honest with me. Have you got another killer? 
SPEAKER2: I have not got any other killers. 
SPEAKER3: Well, at the end of the day, you’ve all, all three of you admitted to at least one killer each. I haven't got one killer. Right. One of you is lying. At least one of you is lying. 
SPEAKER2: I haven't got any killers. 
SPEAKER4: I’ve got the strongest front row. I do not believe I have a reason to lie. 
SPEAKER3: No, you haven't. You've got absolutely no reason to lie. 
SPEAKER4: I've admitted to a killer and I've admitted to a seventy five pound ball. It maybe a low amount.
SPEAKER2: So we've got one one forty thousand 
SPEAKER1: And six thousand. That's over twenty thousand to take through to the next round. I think it's pretty good hand.
HOST: Ange. why should they keep you in the game?
SPEAKER2: Because I have only got one killer. It's the one killer on show. All my other balls are cash balls.
HOST: Richard, why should they keep you in? 
SPEAKER1: I've got one killer and over twenty thousand to take through. 
HOST: Natalie? 
SPEAKER4: I'm here staying honest game, and I've got really good balls and I'm being honest. 
HOST: David? 
SPEAKER3: No killers. Fifteen grand sitting the back row. Enough said. 
HOST: All right, we know where three of the killer balls are. There's one killer ball that could be anywhere. There's a lot of money claimed on the back rows. This game is about bringing through the cash and getting rid of the killers. You have one vote each. Someone has to leave golden balls and you've got to vote now. 

[CUT]


HOST: Hi there. Welcome back to Golden Balls. The balls are in. Our four players have decided who is about to leave the game and see their golden balls binned. There's one killer on show. Two have been admitted on the back rows, so that leaves one missing killer ball. Let's find out where it is.It's time to reveal who's been chosen to leave. The first vote is for... Richard. The second vote is for… Richard. The third vote is for… Ange The fourth and final vote is for... Richard.
HOST: They've thrown you to the lions. Richard, you were claiming a killer ball, if I remember correctly. 
SPEAKER1: That's absolutely correct. Jasper, it’s killer ball. 
HOST: Now, this one, you first of all, you said fifteen, and then when I asked you to check, you did say fourteen. 
SPEAKER1: Uh-huh.
HOST: Is it fifteen? Is it fourteen, or is it another killer? 
SPEAKER1: Quite near to fourteen.
HOST: So fourteen. Shot yourself in the foot a bit there.
SPEAKER1: I think so 
HOST: Natalie, you were admitting a killer. Yes. I think it's in that ball
SPEAKER4: There's the killer. 
HOST: Okay. And seventy five pounds. I’m sure you were telling the truth.
SPEAKER4: Yeah, all honest 
HOST: From Coventry. And you were admitting no more killers.
SPEAKER2: No more killers. 
HOST: Obviously you haven't got them. But 1500. Is that there? 
SPEAKER2: That one there, Jasper. 
HOST: Now then, this will be interesting. Is this ten thousandor was it a porky?
SPEAKER2: It was a porky, I'm afraid. I just forgot a zero. 
HOST: Oh, easily done. 
SPEAKER2: It had to be done, I'm afraid
HOST: Dave. Five thousand pounds. Is that there? All right. Have you done an and or have you done an athlete? Is this fifteen thousand? 
SPEAKER3: It's a chance, Jasper. And guess what 
HOST: It is. Wow. Hey. Em, all right, Richard. Suddenly you're going to have to leave us before we go. Bin, your golden balls, please. Two killers. They won't be sorry to see those go. Six thousand. Richard, you binge your balls, but you're out the game. Thanks for playing. 

[ROUND 2]

HOST: Well, they didn't buy his story. So, sadly for him, Richard is out of the game. The twelve remaining balls go back into the ball machine and the Golden Bank adds two more mystery cash balls. And as always, the delightful Amanda has a new killer ball ready to spoil the party. And that goes into the ball machine. So there's now more money in the game, but there's also one more killer. You now have fifteen golden balls in play, killers in cash. Each of our players now receives five balls at random. Two for the front row and three for the back. We started with four players, we're down to three, soon to be two. Dave, you are very quick on Richard there. I mean, I know you're an investigating police officer. You were very quick to spot that he went from fifteen to fourteen. He changed his evidence.
SPEAKER3: Of course he did. 
HOST: And you found out? 
SPEAKER3: Yeah, very much so. 
HOST: I can tell you that the amount of money who have bought through from the first round is thirty eight thousand seven hundred twenty-five pounds. Feeling good? You only bought through two killers, which was fantastic, because you could have bought through four if you'd have got it wrong. You bought through two killers. We've added one. So there's three killers to find. They can be anywhere. And remember, there is an eleven thousand pound ball and a fifteen thousand pound ball coming through from the first round. Natalie, what have you got on your front row this time? You were fortunate last time. 
SPEAKER4: I've got nine thousand pounds, I think might be a new ball. And I've got fifteen thousand pounds, which I had in the last round.
HOST: Dave, what have you got on your front row? You were killer free last time, weren't you? 
SPEAKER3: Oh, it's number one. 
SPEAKER3: Okay, well, there's three of them out there. Oh, dear
HOST: That’s tough. It all depends on what's on your back row. Ange, what have you got on your front row? 
SPEAKER2: I have two hundred pound on that one and three hundred fifty cash ball on that one. 
HOST: All right, we have two killers on show. There's one missing. You each have three golden balls on your back row. This time, as before, I'm going to ask you to have a look at them. They're for your eyes only. I'll come back and see what you claim that they are. 
SPEAKER2: If I get through to the finals today, I'd ideally like to split, but I just hope I'm not forced into a uturn.
SPEAKER3: Everyone trusts the police. They're going to trust me in the final, but I'm going to steal. 
SPEAKER4: I'm a keen raverand I'll go raving mad if someone steals money from me today in the final
HOST: We have two killers on show, but there's still one to find. As I said earlier, very important aspects of golden balls are trust and belief. No matter how much money you take through, if someone's going to steal off you, it's worthless. So you have to make up your mind who you can trust. Ange, what have you got on your back row? 
SPEAKER3: I have on my back row, the eleven thousand pound ball from the last round. 
HOST: Right.
SPEAKER2: So I'm well-assured that’s my center ball, I have seventy-five pounds and I do have the last remaining killer ball. 
HOST: So you've got the remaining killer ball? 
SPEAKER2: I have.
HOST: You claim to have the eleven thousand pounds. 
SPEAKER2: I don't claim, I know I have just right. 
HOST: And the seventy-five pounds.
SPEAKER2: I do, yes. 
HOST: Natalie
SPEAKER4: I've got all cash balls, which I'm sure have worked out, because all the killers have now been claimed. I've got five thousand pounds in this ball, one thousand six hundred, which I think is a new amount in this ball, and three thousand pound in this ball. It's all really good money. I really want to find someone who I can trust to take them with me
SPEAKER2: It would be me, actually, because I've got all cash balls, apart from one. 
SPEAKER3: Absolutely not
SPEAKER3: I have
SPEAKER2: You haven't, because I've got the eleven grand ball sitting there. I've got the one thousand four hundred pound ball, which I believe
SPEAKER3: You’ve also got two killer balls
SPEAKER3: And I've got fifteen hundred pound ball there. Yes, I've got two killers. But who was honest in the last round and who wasn't? I'm here to split. There's absolutely no doubt about it. I'm here to split the money. 
SPEAKER2: But you've got more killers than anybody else, Dave.
SPEAKER3: I may have, but I've got that eleven grand ball 
SPEAKER2: And that’s money, you haven’t got that eleven thousand pound ball ‘cause I’ve got it. 
SPEAKER3: You haven't.
SPEAKER2: And there was only one from the last it is sat there. As true as I'm sat here. It is there
SPEAKER3: And that's a lie.
SPEAKER2: As true as I'm sat here, Dave, it's there. You've got two killers on that front row. 
SPEAKER3: I might have two killers,that's obvious. I've got the eleven grand ball, fourteen thousand cash ball and
SPEAKER2: All cash balls that’s claimed are sat here, I promise you
SPEAKER4: I'm not even straightdown, I'm not even that bothered, to be honest with you, about the killers, because at the end of the day, we're going to be taking through one or two killers, which isn't that much, to be honest with you. The main thing that I want is someone who's going to be honest, because I'm a very honest person.
SPEAKER2: Well, I had to lie in the last round because I had to be. 
SPEAKER3: Exactly, exactly, I’m here to split all the way. Absolutely no doubt about that. That's eleven grand. That's fourteen hundred
SPEAKER2: That can’t be 11 grand because 11 grand are sat there
SPEAKER3: That's fifteen hundred. 
SPEAKER2: No, he's lying. Natalie, I swear to you, if you believe him… He’s telling you the porky ball
SPEAKER3: Natalie, I can look at you straight in the face and say, that's eleven grand ball
SPEAKER2: He’s telling you the porky ball, Natalie 
SPEAKER3: That's the fifteen hundred, 
SPEAKER2: If you believe him, you will regret it.
SPEAKER4: I really want to believe
SPEAKER2: I, wow, I promise you 
SPEAKER3: Natalie, I’m here to split, Ange is not. She is here, she will steal the money. 
SPEAKER4: Why didn't you jump in when she claimed it
SPEAKER2: Exactly he didn't jump in, he waited.  
SPEAKER4: It was quite, quite a bit before she came. 
SPEAKER3: Because, because, because Jasper asked him- her first. That’s the only simple reason
SPEAKER4: You jumped in, you jumped in. You allowed her to say, you allowed her to explain and then you jumped in. If that was me, I'd have gone, Excuse me. No, you haven't.
SPEAKER2: I have that even though you’ve got it, there’s no arguing that. 
SPEAKER3: But, but it was her own decision. You got a question yourself. Who was honest in the last round? 
SPEAKER2: Yeah. And you'd have done exactly the same, Dave, if you had rubbish your lies like I did.
SPEAKER3: And did I. That's not my problem. That's your problem. I've got the eleven grand ball sitting there.
SPEAKER2: David, you've got two killers sat on the front row. 
SPEAKER3: I have got two killers, obviously, but I've got the eleven grand ball sitting there. I've been proven in the show to be honest, and you haven't.
SPEAKER2: I have. And I can promise you 100% that eleven thousand pounds
SPEAKER3: Absolutely not 
SPEAKER2: And I'm looking you smack bang in the eye. 
SPEAKER3: Absolutely no way.
SPEAKER2: And I will split in the final round 
SPEAKER3: There's no way you've got that eleven grand ball. 
SPEAKER2: Exactly. 
SPEAKER4: Can you look me in the eyes and tell me you've got the eleven thousand pounds? 
SPEAKER3: I've got the eleven grand ball.
SPEAKER4: Can you look me in the eyes and tell me 
SPEAKER2: It's there. Sat straight in the middle. eleven thousand pounds there. Natalie
HOST: Ange, why should they keep you in the game? 
SPEAKER2: Because, yes, I admit I fibbed in the last round about an amount, but this time I am 100% being honest. I have that elven thousand pound ball. 
HOST: David, why should they keep you in the game? 
SPEAKER3: I was honest in the first round. I got okay, I'm two killers, but I have that eleven grand ball sitting in the back row.
HOST: Natalie
SPEAKER4: I've got all cash and I'm here to split. 

[CUT]

HOST: Welcome back to Golden Balls. Our three remaining players have made their decision. It's time for one of them to leave the game and see their golden balls binned. According to Dave, Ange is lying. According to Ange, Dave is lying. According to Natalie, she's in a quandary. Let's see which way she's jumped. It's time to reveal who's survived and who's been binned. The first vote is for Ange. The second vote is for Dave. The third and final vote is for Dave. You are claiming one four hundred on your back row. Is that there, Dave? Fifteen hundred pounds. Is that there? Now, this is what Natalie's waiting to see. You were both claiming the elven thousand pound ball. One of you is lying. David, what is it? One thousand. 
HOST: Ange, you claimed a killer ball.
SPEAKER2: I did. Jasper there it is. 
HOST: Seventy-five pounds.
SPEAKER2: There it is. 
HOST: Now then, what could this ball be? Let me see 
SPEAKER2: I wonder. There it is.
HOST: Eleven thousand pound ball. Natalie, sixteen hundred pounds
SPEAKER4: Yeah
HOST: Three thousand pounds. And, of course, the missing five thousand pounds. 
SPEAKER4: All cash
HOST: Well done. Bad luck, Dave. Always tough with two killers on the front. Exactly. Before we go, can you bin your golden balls, please? One thousand, fourteen hundred. Six thousand. And they won't be sorry to see the two killer balls going. Bin the balls, but you're at the game. 
SPEAKER3: Thanks, Jasper. Cheers.
HOST: Unfortunately, Dave goes home with his lucky underpants at half mast. Finally failed him. Ten golden balls remain in play. Nine cash balls and one killer ball. They're going back into the ball machine. But the bad news is Amanda has a new deadly killer, which he's putting in amongst them. But somewhere in those eleven balls, we know, are the five cash amounts that can make up your maximum potential win today. We've seen them already.They are Three thousand, five thousand, nine thousand, eleven thousand and that fifteen thousand pounds golden ball. Natalie and Anne, you've seen off the competition, but now you have to play together to build up as much cash as you can. It's time to play Spin or Win. Here you are. Finals of golden balls. 
SPEAKER4: Yeah
SPEAKER2: Absolutely.
HOST: It's been sort of straightforward because Richard dropped a bit of a googly in the first round. 
SPEAKER4: Yeah.
HOST: And then poor old Dave had two killers on the front row. So, but you were in a bit of a quandary about who to choose. 
SPEAKER4: It was difficult, but I did end up believing Anne.
SPEAKER2: Good, glad you did
SPEAKER3: Luckily, it was right.
HOST: Yeah. 

[ROUND 3]

HOST: I can tell you that your highest possible win today is fourty-three thousand. 
SPEAKER4: Brilliant.
HOST: That's good, ah? 
SPEAKER4: Brilliant
HOST: And you only bought through one killer. We've added another one. There's only two killers to find. If you can eliminate them in the early rounds of bin and win, you could be on for some very good money here. We're going to play Bin or win. Each player takes it in turn to pick a ball to Bin and a ball to win. The ball to Bin that you choose goes out of the game altogether, whether it's a killer ball, which would be great, or it might be the fifteen thousand pound ball that you choose, but it has to go out of the game. 
SPEAKER4: Yeah
HOST: And then you pick a ball to win. The ball to win goes into one of these top golden five slots here, and the five balls make up your jackpot total for today. If at any time you pick a killer ball to go into one of these top slots, it knocks a naught off your accumulated total. So if you had that sort of nine thousand pound ball there, that's out an elven thousand pound ball,that's twenty thousand. Pick a killer and the twenty thousand goes down to twenty hundred. Now, Natalie, you bought through the most amount of money, so you get to choose first. 
SPEAKER4: Okay
HOST: Pick a ball to bin.
SPEAKER4: Right. Do you have any kind of, like, game plan, any feelings towards them? 
SPEAKER2: No. They're all gold. 
SPEAKER4: Yeah, they are all gold. Hopefully they will go for this one or a low amount, whichever 
HOST: Alright, show us what you've chosen.
SPEAKER4: Seventy-five pounds. 
ANGE: That’s good
SPEAKER4: That's not bad. 
HOST: Pass the bin. Now, choose a ball to win.
SPEAKER4: I might go for the dead centre one. 
HOST: My job. Got to do something right here, you know. And, Natalie, your first winning golden ball today is two hundred pounds.
SPEAKER2: All cash
SPEAKER4: Definitely.
HOST: Ange, your turn. Pick a ball to bin. 
SPEAKER2: Okay. Have you got any feelings? 
SPEAKER4: No, just go for whatever you think. 
SPEAKER2: Okay, well, I'll go for that one, I think. 
HOST: Show us what you chose. It's a killer? Terrific. 
SPEAKER2: Oh, I’m sorry
SPEAKER4: Oh, no, no, it’s not too bad
HOST: Has to be binned. Now, pick a ball to win. 
SPEAKER2: Any feelings? 
SPEAKER4: No, honestly, just go for whatever 
SPEAKER2: Honestly, I just have no feelings whatsoever. It goes to the balls anyway. 
HOST: All right, your second winning golden ball today is… It's a Killer. 
SPEAKER2: It’s good 
HOST: Knocked it all out off the two hundred pounds. So you're now looking at £20 for your jackpot total. But you have eliminated one of the two killers. Your highest possible win is now thirty five thousand twenty.
SPEAKER4 and SPEAKER2: Okay
SPEAKER2: Could have been worse. 
HOST: Back to you, Natalie. Pick a ball to bin.
SPEAKER4: I'm edging towards one of these two. 
SPEAKER2: Okay. 
SPEAKER4: Have you got any kind of vibe towards either one of them?
SPEAKER2: No, not at all.
SPEAKER4: I don't know this because I don't know. I think I'm going to go to this. 
SPEAKER2: Yes.
HOST: Okay, killer ball. It's cash all the way. 
SPEAKER4: It’s nine grand. Sorry
HOST: Has to be Binned. 
SPEAKER2: Never mind
HOST: Oh, dear. The eleven thousand is still out there and the fifteen thousand is still out there. And also a 3000 pound ball. But of course, there is a killer.
SPEAKER4: Yes. 
HOST: Pick a ball to win. 
SPEAKER4: Oh, my God. Do you have a feeling there? I don't know what that might be. 
SPEAKER2: That might be the killer now. At least if it is, then it's too 
HOST: Well, it's out and then chance of getting those high cash ones in. But let's hope it's a big cash ball. Your third winning golden ball today is three hundred fifty. 
SPEAKER2: Oh alright
HOST: You did have twenty pounds, you now have three hundred seventy pounds. Right, back to you, Ange. 
SPEAKER2: Right
HOST: Pick a ball to Bin. You're looking for that missing Killer. It would be fantastic if you could get rid of it. 
SPEAKER2: I'm just edging towards that one. 
SPEAKER4: Okay.
SPEAKER2: Yes. 
HOST: Oh, yeah.
SPEAKER4: Brilliant
HOST: Very good. Very, very good. 
SPEAKER2: Be gone, be gone
HOST: All right? Four balls left. All cash balls. There's a fifteen thousand pound ball, an elven thousand pound ball, a three thousand pound ball. Get that fifteen thousand pound ball and the elven thousand pound ball into those two slots and you're on for twenty-six thousand three hundred seventy pounds. And you had the winning touch last time, do it again and we're onto some big money. Pick a ball to win. Happy with that choice?
NATALIE: I'm happy. 
HOST: Right, we all know what you want this ball to be. We also know what's coming.
ANGE: Ah
HOST: A break. 

[cut]

HOST: Golden balls. Before the break, Natalie and Ange had accumulated three hundred seventy towards their golden five. However, they have eliminated both killer balls that they bought through. So it's cash all the way. There's a fifteen thousand pound ball out there and an elven thousand pound ball. And just given me this. If it's fifteen or the eleven, you're on for twenty-six thousand three hundred seventy pounds. Your fourth winning golden ball today, ladies, is your luck’s changed, elven thousand pounds. Natalie. 
SPEAKER4: Yes. 
HOST: You have three golden balls left. There's a fifteen thousand pound ball, there's a. Three thousand pound ball and there's a sixteen hundred pound ball. Get it right, you're on for twenty-six thousand three hundred seventy. But if it all goes back, end up, you're still on for twelve thousand nine hundred seventy pounds. So there's some very, very good money waiting for you two to decide what to do with. However, first and foremost, you have to pick a ball to bin.
SPEAKER2: Your instincts have been quite good. Have you got anything? 
SPEAKER2: I keep looking at that one, but only to stayaway 
SPEAKER4: From it between the two of them 
SPEAKER2: To bin, you mean? 
SPEAKER4: Yeah. Or we need to keep
SPEAKER2: I keep looking at that one as if they stay away from it. I don't know why 
SPEAKER4: I'll give you this one. 
SPEAKER2: It's your choice. Obviously. I'll go with whatever you want to anyway. 
HOST: Right. You don't want the fifteen thousand. 
SPEAKER4: Right. It’s the sixteen hundred.
HOST: Very good. 
SPEAKER2: Good job! Yes
HOST: You have one more choice to make. This choice is worth twelve thousand pound. No pressure. 
SPEAKER4: Now, you were saying last time, like, to stay away from this one. 
SPEAKER2: Mhm
SPEAKER4: Do you still feel stay away from it? Because I kind of want to grab it
SPEAKER2: But I wanted you to stay away from it so you didn't bin it.
SPEAKER4: Yeah. So should we go for that one? Yeah?
SPEAKER2: Yeah, deal. 
HOST: You're on for some good money, whatever happens. Your final winning golden ball today is either three thousand pounds or fifteen thousand pounds. The difference between a jackpot of twenty-six thousand three hundred seventy and fourteen thousand three hundred seventy. Both of you have chosen this ball and it is 
SPEAKER4: Yes
HOST: Fifteen grand. 
SPEAKER4: There’s three grand
HOST: What do you want to do with it? Put it in?
SPEAKER2: Yeah. 
HOST: How often can you be pleased to throw away three steps? Natalie Ange, you are going for a jackpot now of twenty-six thousand three hundred seventy. You've got it. The golden question is, can you keep it? It's time to play Split or steal. Thank you. Ange, Natalie, you have two final golden balls each. They're not cash, they're not killers. You each have a golden ball with the word split written inside. You each have a golden ball with the word steal written inside. I will ask you to check both the balls to make sure you know which is the split ball and which is the steel ball. I will then ask you to make a conscious choice of picking the split or the steal ball. Neither of you will know what the other has chosen. If you both choose the split ball, you will split today's total of £26,370 and you will both go home with thirteen thousand hundred eight-five pounds. Okay? If one of you decides to steal and one of you decided to split, whoever chooses the steel ball goes home with all the money, the whole jackpot of twenty-six thousand three hundred seventy. Whoever chooses the split ball goes home with nothing. If you both decide to choose the steel ball, it's quite simple. You both go home with nothing. Can I ask you now to check your two balls to make sure you know which is split and which is steel. Be very careful as you look at them. Don't show each other. Are you sure you know which is the split and which is the steel board? You sure? 
SPEAKER4: Yeah. 
HOST: Before I ask you to choose either split or steal, I always give you a couple of minutes to discuss what you would like to do.

[SPLIT/STEAL]

SPEAKER2: Okay. Well, we've come this far. 
SPEAKER4: I have been completely honest with the game. 
SPEAKER2: You have. Yeah.
SPEAKER4: And I hope that you can completely trust me. 
SPEAKER2: I want to trust you 100%. 
SPEAKER2: I wanna to trust you 100%.
SPEAKER4: Thirteen thousand will do exactly what I want it to do.
SPEAKER4: I couldn't even dream of stealing that. 
SPEAKER2: I know I figged in the first round, but I had to. But I've been totally honest since, and I really do want to share with you. Yeah? 100%. 
SPEAKER4: Yeah, definitely.
SPEAKER2: I want to trust you and you. 
SPEAKER4: And you. 
SPEAKER2: Okay. 
HOST: Ange, Natalie, choose either the split or the steel ball. Now, hold it and then count to three, and I want you to show us what you have chosen. Three, two, one. Split or steal? Split.
HOST: Wow. 

[END]

SPEAKER2: Thank you
HOST: How is that? 
SPEAKER2: Thank you, Jasper
HOST: Natalie! You're going home with £13,185. Ange, you're going home with thirteen thousand one hundred ninety-five pounds. Well, how many of you saw that coming? Honestly, what's going to happen next time? Tune in and you'll find out. See you then. On the next game of Golden Balls
SPEAKER2: I chose to split ball because it's like Natalie. I wanted to play an honest game and I didn't want to go away with no money at all.
SPEAKER4: Winning over thirteen thousand is absolutely amazing. I'm shaking so much. I'm so happy. Thank you so much.
