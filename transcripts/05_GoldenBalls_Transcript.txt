HOST: [CUT ]Some people believe in ghosts.
SPEAKER2: [CUT ]Catch me off on me birthday, can you? It’s- it's got to be one to remember.
HOST: [CUT] Some people believe in UFO's.
SPEAKER1: [CUT] I'm all sweating. (laughter) I always sweat.
HOST: [CUT] Some people will believe anything because the truth is out there.
SPEAKER3: [CUT ]If anybody's got the other killer, they need to say.
HOST: [CUT] Put it in here because this is Golden Balls.
(intro + clapping)
HOST: [CUT] Thank you. Hi, welcome to the show. Where you can win a fortune, but only if you have got the balls. Yep, it's Golden Balls. Once again we have four players looking to block their way to the big money. An area sales manager, an arts officer, a tribute artist and a town crier. And who’s going to take their luck. And here's the star of the show. Yep, it's the golden balls, and there's a hundred more just like this one behind me up in the golden band. They’re ready to come out to play. We want them to come out to play, so let's start the ball machine.
HOST: [CUT] Ah, this is the bit I love, watching all that money swirling around up there because we all know that one of those golden balls we can see contains an incredible seventy-five-thousand pounds.
HOST: [CUT] They circulate (inaudible). But in this game, where there's cash, hey, there’s killers and the queen of the killers is Amanda. And there she is, doing what she does best, sticking four of her killer balls in into the ball machine. And so there they are, the first sixteen golden balls of today's game, cash and killers. And as always - each player receives four balls upon (inaudible). That's two for the front row. And two for the back.
[ROUND 1]
HOST: Hi everybody. Welcome to golden balls! It’s lovely to see you, lovely to see you! You've got sixteen golden balls out there. Four of those balls are killer balls. We don't know where they are. Twelve of them are cash balls. How much is inside? We don't know. Let's find out. Our first player today is Guy from Kidderminster. Hello Guy, hello Guy (inaudible) good to have you around, good to have you around.
HOST: What do you do Guy?
SPEAKER1: I am actually a Elwood Blues and a Blues winners tribute band.
HOST: Yeah?
SPEAKER1: Mmhm, yeah.
HOST: Yeah, but how do you get into something like a Blues band?
SPEAKER1: Basically I always liked singing and I was just better to uh leave for Ibiza, get a bit of sun.
HOST: Yeah.
SPEAKER1: To become uh an Elwood out there. Some- some of the guys, Jake. And uh- uh- my Jake stopped me going and said well, let's do it here. And so uh six years later, um, they're still doing it.
HOST: You've been doing it six years?
SPEAKER1: Yeah, yeah, yeah, yeah.
HOST: Yeah, I know you're uhm a massive film fan -
SPEAKER1: - Yeah -
HOST: - you love your film stuff -
SPEAKER1: - Yeah.
HOST: - with an encyclopaedic knowledge I was told.
SPEAKER1: Pretty much.
HOST: Hey, I asked you a question earlier on today and -
SPEAKER1: - Yeah -
HOST: - I said to you, who was the star of the film “Jane and the lustre”.
SPEAKER1: You know what? I've been racking my brain and I still can't picture who’s -
HOST: - It was a bloke called Sam Jones, but there was a couple of very famous people in the film supporting him. Lord Adams.
SPEAKER1: Yes!
HOST: Yes, somebody else?
SPEAKER1: Don’-
HOST: - Come on, come on (inaudible)
SPEAKER1: Christopher Cary.
HOST: Yeah! (inaudible) Excellent!
SPEAKER1: (inaudible)
HOST: OK, uhm, Guy, first person up today, show us what's in these two golden balls on your front row.
SPEAKER1: Okay, (inaudible) Right, on the first one. Gonna open. Uhhh, uhhh. So I’m at-
HOST: - There’s loads of them there.
SPEAKER1: Aemh, my second one, open up, is sixteen-thousand (inaudible)
SPEAKER1: Yeah, alright. I can’t help myself. Sixteen-
HOST: - Sixteenhundred.
SPEAKER1: Alright (inaudible) I know, I haven’t (inaudible)
HOST: Our next player is Louise from Doncaster. (inaudible)
SPEAKER2: How you’ve been?
HOST: I’m doing well, lovely to have you here.
SPEAKER2: Thank you for having me.
HOST: That's a pleasure. And what do you do?
SPEAKER2: Basically I sell tools to builders merchants and garland centres throughout the East Midlands area.
HOST: Right. It's a large area of Nottingham and Leicester and Manchester.
SPEAKER2: It is, yeah, but it's great because it keeps you busy and uh it's the variety that the customers are different, the people are different from various areas, so there's sort of a different challenge in each area.
HOST: Uhm, we were talking of you, you considered modelling at one stage-
SPEAKER2: - Uh-mmh -
HOST: - because you had a few photos in a few magazines.
SPEAKER2: Uhm, yes.
HOST: Which ones?
SPEAKER2: Uhm, FHM, High Street honeys. I was in there a couple of times.
HOST: Alright, Yeah.
SPEAKER2: And uhm, and also in in Nuts magazine as well.
HOST: What?
SPEAKER2: In Nuts magazine.
HOST: Nuts magazine?
SPEAKER2: Yes. A clothes mag.
HOST: Oh, all of these, oh uhm-
SPEAKER2: - Have you not heard of Nuts?
HOST: This is a family show, behave yourself!
SPEAKER2: It’s-
HOST: - Alright then, your second person up. What have you got on your front row Louise?
SPEAKER2: OK, first of all is ten-thousand pounds.
HOST: Oh! (clapping) Second ball.
SPEAKER2: Second ball’s one-thousand-seven-hundred.
HOST: Very nice!
SPEAKER2: Thank you.
HOST: Our third player is Peter from Stafford. Welcome to the show.
SPEAKER3: Good to see you.
HOST: Peter, you're our first ever town crier on the show.
SPEAKER3: Yeah, town crier, toastmaster and of course Santa Claus.
HOST: Oh, of course, yes. How do you get into town crying? How do you get the job?
SPEAKER3: Well, there was an address in the paper and it just said he wanted town crier, and uh my wife said you got a big mouth, go for it.
HOST: And you're uh a proud member of the uh the-
SPEAKER3: - Loyal company of town criers
HOST: I need - You get together in that meeting?
SPEAKER3: Uh, we have different competition. Yeah. Fantastic.
HOST: Must be bloody noisy.
SPEAKER3: What have-
HOST: - Peter! Hey, Peter come on (inaudible) in your ear! Your missus is having an affair! (inaudible)
(laughter)
HOST: Peter, what have you got on your row?
SPEAKER3: Right, let’s see what we got here, let’s- Oh, only a small one, but not a killer. Two-hundred-
HOST: - two-hundred pounds, and?
SPEAKER3: And here we have – uh oh – thirteen-thousand! Yeah, that’s great to (inaudible)
HOST: Somebody's building up everybody.
SPEAKER3: Yes.
HOST: It would be a very big money game. And finally our last place, Julie from Manchester. What do (inaudible)?
SPEAKER4: Hello (inaudible)
HOST: What do you do?
SPEAKER4: Uhm, I’m an arts officer. And that means that I work with local communities and I work with organisations to get more people involved in doing the arts in whatever art form that might be.
HOST: You were saying your worst job was working for Butlin in seafood.
SPEAKER4: Yes, it definitely was, that was definitely the most terrifying thing I've ever done.
HOST: What did you have to do?
SPEAKER4: I had a goal bingo and I had to call Bingo to professional Bingo Players. They travel down there for their week’s holiday to play bingo and do nothing else, and it was very scary.
HOST: And uh you went to Birmingham University, of course.
SPEAKER4: I did I did. Yeah.
HOST: I knew there was a quality in that.
SPEAKER4: Well, you can tell (inaudible)
HOST: And you're fascinating. You were doing the dissertation on women's …?
SPEAKER4: Stand up comedy. Yeah –
HOST: - Yeah!
SPEAKER4: And I, I've sent a lot of questionnaires about and I asked people to do interviews and talking. The best one I did was with Joe Brown.
HOST: She invited you round?
SPEAKER4: She did! She liked, she was very generous because I was just some, you know -
HOST: - Yeah -
SPEAKER4: - twenty-one-year old student and she invited me round to her house and uhm baked teeny cakes!
HOST: Julie, the last person up today, what have you got on -
SPEAKER4: - OK. I've got one-thousand-four-hundred.
HOST: Very nice.
SPEAKER4: I’ve got eight-thousand.
HOST: Wow!
HOST: We have over thirty-five-thousand pounds on the (inaudible) Only one killer, there's three killers to find. You each have two golden balls on the back, you don't know what they are, I'm gonna ask you to have a look at them. There for your eyes only and I'm gonna come back and find out what's lurking in the back.
SPEAKER1: [CUT] There’s Elwood Blues, in a tribute band, I often use the line “Everybody needs somebody to love”, so I'm hoping everybody's gonna love me.
SPEAKER2: [CUT] I work in predominantly a male environment and I do not back down easily, so watch this space.
SPEAKER3: [CUT] This is a game, so I would love to get to the final. Will I do enough?
SPEAKER4: [CUT] In my life, I've travelled a lot and I plan to go all the way to the final set.
HOST: You know something that we don't and that’s what you each have on the back two balls. Uh, We have one killer on show so you've got to find where the other three killers are. Hey, Guy! What have you got in the back?
SPEAKER1: Well, that's a very small one. That's only twenty quid -
HOST: - Yeah? -
SPEAKER1: - but that is twelve-thousand.
HOST: Julie, have you got any killers on the back?
SPEAKER4: I have actually another (inaudible)
HOST: Alright, OK.
SPEAKER4: I've got uh one killer and I've got five-hundred pounds.
HOST: Peter. Any killers?
SPEAKER3: No, no.
HOST: No? Alright.
SPEAKER3: Seven-thousand-five-hundred
HOST: Yeah.
SPEAKER3: And one-thousand-one-hundred.
HOST: Luise?
LUISE: OK, well it looks like honesty is the best policy, so I'm gonna go down that route as well. I'm gonna start off on a positive uhm alongside of what I've got and I've got nine-hundred pounds there.
HOST: Yeah.
LUISE: And then I do have a killer. So there is someone telling a little white lie.
HOST: So there is someone untruthful there, because there's a killer missing. Who you think it might be Luise?
LUISE: Uhm, what did you say you got again Guy?
SPEAKER1: Uh, twelve-thousand in there -
LUISE: - Uh-hmm -
SPEAKER1: - and just twenty quid in there.
LUISE: I'm looking at either Guy or Peter and I'm sort of tending towards Guy at the moment as -
SPEAKER1: - yeah, yeah, yeah it -
SPEAKER4: - It’s not actually to do with the killer on the front, it's to do with, it's something with your body lang. I think you might be lying about one of the balls.
SPEAKER1: I’m all sweating. (laughter) I always sweat as -
SPEAKER2: - And I think you paused slightly when it was like it’s only a low amount -
SPEAKER1: - Who’s gonna have the other one (inaudible) -
SPEAKER2: - It’s twenty-five pounds -
SPEAKER1: No it was twenty pounds.
SPEAKER2: It was twenty pounds, yeah yeah. And then the twelve-thousand, uhm (inaudible)
SPEAKER1: I think it’s, I I could have said that was a higher ball, but I want to be honest with you, as far as I can, but that is twenty pounds.
SPEAKER4: So I don't think you are. I can understand why you wanna lie -
SPEAKER1: - Yeah, yeah, absolutely.
SPEAKER4: I think you’re not telling -
SPEAKER1: - but this is it, it’s a tough ball, yeah we, you said you gotta killer, you said you gotta killer, there’s another one missing.
SPEAKER3: The main thing we want at the end is to be able to trust each other and go through.
SPEAKER1: Yeah.
SPEAKER3: This is why I feel that if anybody's got the other killer, they need to say.
SPEAKER1: Yeah, yeah -
SPEAKER2: - So you -
SPEAKER1: - yeah, yeah.
SPEAKER2: So they say - (inaudible) You’ve not got the other killer then?
SPEAKER3: Yeah.
SPEAKER4: Are you sure? Are you actually sure, Peter?
SPEAKER3: Oh I don’t-
SPEAKER2: -There’s anything I don’t need to ask -
SPEAKER3: - I’m, I'm looking straight at you. Thirteen-thousand there, I don’t need to lie.
SPEAKER1: I'd love to go through to the next round as well. I really, really would.
SPEAKER4: Uh-uh.
SPEAKER1: I mean we all would. I mean that's how it works.
SPEAKER2: But that’s (inaudible) killer again, Julie?
SPEAKER4: No, I think I think it's that one, and uhm, yeah it’s that one.
SPEAKER2: Ah-ha.
HOST: Who’s the missing killer? Guy? (inaudible)
SPEAKER1: I'm tending to Julie. I know, I know -
SPEAKER4: - I think, I think they’re trying to deflect of yourself, I’m really not lying. I was the first person to admit having a killer about –
(all talking over each other)
SPEAKER2: - And Julie had two killers. She wouldn’t go like that to remember which one was the, the killer in the first place, cause she -
SPEAKER1: - no, I’m gonna -
SPEAKER2: - she -
SPEAKER1: - yeah, yeah, yeah -
SPEAKER3: I completely trust you.
SPEAKER4: OK.
SPEAKER3: The way I feel -
SPEAKER4: - Right -
SPEAKER3: - I completely trust you.
SPEAKER2: Uh-hu, about, what about -
SPEAKER3: - I I must say. That with the money you've got at the front, you could get away with two killer balls.
SPEAKER1: Mmh-hm.
SPEAKER3: So if you've got them, say so.
SPEAKER2: That is not a killer. I I can’t say which -
SPEAKER3: - Okay you know that's fine, that's fine.
SPEAKER2: - when you say to. When it isn’t a killer, that would be absolutely stupid. I am turning towards Guy, unfortunately.
SPEAKER1: Okay, well I feel struggle, because all you got, I've only got low bands and (inaudible)
SPEAKER2: You know you are in quite a weak position -
SPEAKER1: - Oh, I’m -
(inaudible, overlapping speech)
SPEAKER2: So, -
SPEAKER4: Are we saying that -
SPEAKER3: - But also I find yours as well because -
SPEAKER1: - Obviously, I got to go.
HOST: Julie, why should they keep you in the game?
SPEAKER4: Because I'm being completely and utterly honest with all of you and I will be throughout the game.
HOST: Peter?
SPEAKER3: Same, honest. No problem and I've got good cash mat.
SPEAKER2: Mmm-hmm.
HOST: Louise?
SPEAKER2: Absolutely the same. I've got exactly what I said. I've got a nice cash amount and I would love to get to the final. And I will also share the money, which is an appea-
HOST: Guys.
SPEAKER1: I'm just trying to be as honest as possible to council. We also got the killers here. That's twelve-thousand and yeah, it's just twenty pounds. I know there's a killer there, but if you take you through, I promise, oh, we're gonna split.
HOST: Alright. Remember. Someone's gonna leave. And when they go, they take their golden ball with them. Cash and killers. There's one killer on show. Two more are mi-, there’s one missing. Someone has to leave the golden balls, and they're gonna leave after this.
(everyone decides)
HOST: [CUT] Thank you Amanda. The players have decided which one of them is leaving the game. They're all so convicted, almost makes me think that Aman has only put three killer balls in there. Could she have made a mistake? What do you think? Someone's lying, and someone's leaving, we'll find out who after the break.
(break)
HOST: [CUT] Welcome back to Golden Balls, the votes are in, our four players have decided who is about to leave the game and see their golden balls bin. The situation is we can see one killer on Guy’s front row. Louise and Julie have admitted the killer on their back row, Peter claims to be killer free. Someone is lying and now someone is leaving. It's time to reveal who's been chosen to leave. The first vote is for … Julie. The second vote is for … Guy. The third vote is for ... Guy. The fourth and final vote is for ... Guy.
HOST: You’ve been claiming twenty pounds Guy.
SPEAKER1: Ah, oh, yes.
HOST: And that’s it.
SPEAKER1: All of it.
HOST: Oh, it is there. And you're claiming this was twelve-thousand. Is it twelve? Is it less or is it the missing killer? Ahh (inaudible)
HOST: Julie, you admitted a killer?
SPEAKER4: I did.
HOST: Let's have a look.
SPEAKER4: And here it is.
HOST: And final five-hundred pounds, is that it?
SPEAKER4: Yes it is.
HOST: Lovely. Louise, you admitted a killer?
SPEAKER2: I did.
HOST: Is that there?
SPEAKER2: It is indeed.
HOST: Nine-hundred pounds.
HOST: And, yeah-
SPEAKER2: (inaudible) yes.
HOST: And Peter, one-thousand-one-hundred. Remember where it is?
SPEAKER3: I do.
HOST: Alright, let’s have a look at it.
SPEAKER3: One-thousand-one-hundred.
HOST: And is this seven-thousand-five-hundred?
SPEAKER3: Well…
HOST: You weren't telling lies?
SPEAKER3: Yes, it is.
HOST: Oh! Oh yes, (inaudible) Guy, not gonna say. Two killers.
SPEAKER1: Yeah.
HOST: It was a bit shaky on the front.
SPEAKER1: Yeah.
HOST: Was the other one on the bat.
SPEAKER1: Absolutely.
HOST: There you are. You're gonna have to be involved before you go.
SPEAKER1: Uh-hu, yeah.
HOST: Thanks very much.
SPEAKER1: Uh-hmm, yeah.
HOST: Great having you on.
SPEAKER1: Thank you very much.
HOST: See you then.
[ROUND 2]
HOST: [CUT] Oh brother, Elwood is feeling blue (inaudible) Guy, and sadly for him, he's out of the game. The twelve remaining balls go back into the ball machine. And the golden bag adds two more mystery cash balls. And as always, Amanda, the Devil woman with killers on her mind, adds one more killer ball into the ball machine. So there's now more money in the game, but the tool shows one more killer. You now have fifteen golden balls in play. Killers an’ cash. Each of our players now received five more than random. Two for the front row and three for the back.
And then there were three. It's really disappointing, you all told the truth then. This is golden balls. You can't start doing things like that in this show.
SPEAKER2: (inaudible)
SPEAKER4: Having a lifetime.
HOST: Huh. Now then, because you told the truth, damn it, we only bought thr- two killer balls, but we've added one, hah! So there's three killer balls to find. They could be anywhere, but also because you told the truth, damn it, you bought through from the first round fourty-four-thousand-three-hundred pounds. (clapping) Oh no, that’s right (inaudible) You won't have a show if this carries on, including of course the ten-thousand pound ball and the thirteen-thousand pound ball. Julie, what have you got on your front row this time?
SPEAKER4: OK, I've got one-thousand-four-hundred. (inaudible) and I've got one-thousand-one-hundred.
HOST: Peter, what have you got on the front?
SPEAKER3: Here we go. Big amount. Two-hundred.
HOST: Ufff. Woah. Woah.
SPEAKER3: And here we have ahh ten-thousand.
HOST: Ohh yay, oh yay! Louise, what’ve you got?
SPEAKER2: Nine-hundred pounds, which is the same ball that I had in the last round.
HOST: Right, and?
SPEAKER2: Five-hundred pounds.
HOST: What? (inaudible) hmm, no killers showing.-
SPEAKER3: No.
HOST: -This is not going to be so easy is it? You each have three golden balls on the back now you don't know what they are, like the first round. I'm gonna ask you to look at them, and we're gonna come back and find out what's lurking around.
SPEAKER2: [CUT] If I can't go into the finals down Golden Balls I would absolutely love to split the money, but if I get the impression that the other person is gonna steal then I'm inclined to perhaps do the same thing.
SPEAKER3: [CUT] Trust me, I am honest. Split. Yes, I will. When I get to the final, I will kill.
SPEAKER4: [CUT] My four-year-old daughter is at that stage where she asked me why, why, why all time. I won't be able to look her in the eye if I have to explain to her why I've stolen.
HOST: Well, the killer ball d’are be shy today. Three missing in the first round, three missing in the second round. We could call them Lord Lucans if you like.
HOST: Peter, have you found the one?
SPEAKER3: I’ve got one there.
HOST: Alright.
HOST: About it are the thirteen-thousand has gone to me. Thirteen-thousand and one-thousand-seven-hundred. It’s going the same as before, honest all the way.
HOST: Louise, what you got?
SPEAKER2: I actually also have a killer. But I have some very nice one as- eight-thousand pounds. And seven-thousand-five-hundred and my killer is sitting right there. So I only have the one killer and a very nice amount, just under thirteen-thousand pounds which would be fantastic to take through the final.
HOST: Julie?
SPEAKER4: OK. I'm also gonna admit to having a killer.
HOST: I can't believe-, bring some more people in. We can't have all these people telling the truth.
SPEAKER4: I've got a killer, -
HOST: - yeah -
SPEAKER4: - but I've also got a new ball and it's fifteen-thousand pounds.
HOST: Fifteen-thousand pounds
SPEAKER4: And I've got six-hundred-and-fifty in the middle.
HOST: So you've got two new balls have you?
SPEAKER4: No, I think six-fifty was in the last round.
HOST: No.
SPEAKER2: I don't think it was, no.
SPEAKER4: No, I don't -
SPEAKER2: I think six-fifty is a new ball and -
SPEAKER4: - Then I've got two, then I’ve got two new balls.
SPEAKER3: Yes.
SPEAKER4: And has anyone else claiming?
SPEAKER3: No.
SPEAKER4: Anyone else claiming, Peter you have any balls?
SPEAKER3: No.
SPEAKER3: And there we are, we're all being honest, aren't we?
HOST: Well, excuse me while I just go and get a bite.
SPEAKER3: What we've gotta decide now is who we can trust in the next round.
SPEAKER4: And I'd like to say that that would be me. I've got fifteen-thousand and fifteen-thousand is the biggest money ball in play
SPEAKER3: Yeah.
SPEAKER4: And we can take this through. Fifteen-thousand pounds is a lot of cash.
SPEAKER2: And if you were to add just under thirteen-thousand pounds to that. That would be absolutely even better. 
SPEAKER4: Oh-
SPEAKER2: - So can I just say Julie, it's me birthday today.
SPEAKER4: (inaudible)
SPEAKER2: Give me a little bit (inaudible), come on catching me up on me birthday, can you, I- it’s got to be one to remember.
SPEAKER3: How much cash have you actually got altogether?
SPEAKER2: I’ve got eight-thousand and I've got seven-thousand-five-hundred there. That's without calling.
SPEAKER3: Yeah.
SPEAKER2: So yeah, you can see you can see what I've got so-
SPEAKER3: - and -
SPEAKER2: - that's quite nice actually. You know, I've only got the one killer.
SPEAKER3: So imagine, unfortunately, you've got the lowest amount of money out of all of us.
SPEAKER2: What do you have again Peter?
SPEAKER3: I've got thirteen-thousand.
SPEAKER2: Uh-hu.
SPEAKER3: One-seven. And the ten.
SPEAKER2: It’s-
SPEAKER3: -D’you want m-
SPEAKER2: -it’s the one- that a new ball?
SPEAKER3: No, no one-seven I had last time. With no one won one.
SPEAKER2: No, yes, you had one-one, and (inaudible)
SPEAKER4: (inaudible)
SPEAKER2: Yes, so…
SPEAKER3: Uh who had the one-seven before?
SPEAKER2: Uhm, I don't think anybody had the one-seven I think.
SPEAKER3: Somebody have -
SPEAKER2: You had to uhm obviously plucked that out of -
SPEAKER4: Maybe one-seven is a new ball.
SPEAKER3: One-seven-
SPEAKER4: Or maybe no. Sorry. Maybe he's saying one-seven is -
SPEAKER2: - Yeah, exactly -
SPEAKER4: - but actually it's -
SPEAKER2: - only two new cash in round. I’ve got them -
SPEAKER3: That is definitely one-seven and no reason whatever to lie. Those two there,-
SPEAKER4: - I think -
SPEAKER3: - No reason. -
SPEAKER4: We’ve actually got to focus on who it is.
SPEAKER3: I trust –
SPEAKER4: I do not wanna go home with nothing.
SPEAKER3: - with nothing (at the same time)
SPEAKER2: This is a game that we’re all involved.
SPEAKER3: Well right the way down the line, I said what I’ve got, I’ve been honest,-
SPEAKER2: - Uh-hu -
SPEAKER3: - never deviated.
SPEAKER2: Uh-hu, tha-
SPEAKER4: - who we’re gonna trust -
SPEAKER2: Uh, I was just – (inaudible) and that’s, and I was -
SPEAKER4: - me too.
SPEAKER2: I’ve proven that.
SPEAKER3: Yeah.
SPEAKER4: I am not, I am not going on national television to lie. Because it’s- I can’t do it, I just can’t do it.
SPEAKER2: Uh-hu.
SPEAKER4: And I haven’t at all, and I’m so ecstatic that I have not had to lie cause it would have really left-
(all three talking over each other)
SPEAKER3: Where are we actually-
SPEAKER2: -Where are you sort of laying then, laying with me and Peter?
SPEAKER4: I don't know. I don't know because I've changed my mind about both of you. Twice.
SPEAKER3: Uhm-
SPEAKER4: Alright. And I'm, I'm really not sure.
HOST: You have a very, very difficult situation.
SPEAKER2: Mhm-hmm.
HOST: Louise. Why should they keep you in the game?
SPEAKER2: They should keep me in because I do have strong balls and I do have some very nice amounts there. I've only got the one killer, which is fantastic. It's my birthday and I want to be able to hold my head up high when I walk off and feel really great about what I've done in the show.
HOST: Julie.
SPEAKER4: I have been completely honest, as has the rest of you, and I also have the biggest cash amount in the game and my other … amounts are really good as well. They're good solid balls so I think you should keep me.
SPEAKER3: Well, I want to split. I want to be able to take something home. I want to say to the wife. OK. We are going on holiday.
HOST: Alright so then we have-, you have a very difficult situation here -
SPEAKER3: Mhm-hmm.
HOST: You really have to make sure that you're making the right decision.
SPEAKER4: You'll never see it a steal ball with me.
SPEAKER2: And me neither
HOST: You have to make that decision now.
(they decide)
HOST: [CUT] The votes are in our players have decided. I apologise to viewers who tuned in expecting to see Golden Balls. Instead, we've ended up with the honesty channel. Is honesty the best policy? Don't ask me. More people like these and I’m out of a job. See you after the break.
(break)
HOST: [CUT] Welcome back to Golden Balls. Before the break, it looked to me like everyone was telling the truth about what they had on their back row. Were they? We're about to find out because it's time to reveal who survived and who's been binned. The first vote is for ... Peter. The second vote is for … Louise. The third and final vote is for ... Louise.
HOST: Ah Louise. You were admitting a killer.
SPEAKER2: Mhm-hmm.
HOST: Let's have a look. (opens ball #1) Seven-thousand-five-hundred? (opens ball #2). And there you are. Peter, you admitted the killer.
SPEAKER3: And of course it’s there.
HOST: It’s there.
SPEAKER3: Of course.
HOST: Seventeen-hundred? A little bit of dispute on that.
SPEAKER3: I’ve got one-thousand-seven-hundred, yeah.
HOST: It has to be there.
SPEAKER3: There it is.
HOST: And the (inaudible)
SPEAKER3: Yeah -
HOST: - thirteen-thousand - has to be there.
SPEAKER3: Thirteen-thousand.
HOST: Julie, you admitted the killer?
SPEAKER4: I did. And there it is.
HOST: In the middle six-hundred-and-fifty.
SPEAKER4: Indeed, there it is.
HOST: (inaudible)
SPEAKER4: Well Jasper in fact...
HOST: Oh no…
SPEAKER4: It is fifteen.
HOST: That was a missed (inaudible) you just missed that. But it's been … great having you.
SPEAKER2: It's been absolutely fantastic being here.
HOST: Just bin the balls before you go, that's the killer ball, three-hundred. There you go. Pick you up. Eight-thousand. Seven-thousand. Not too pleased to see that go-
SPEAKER2: - Uh-hu –
HOST: -Louise, you've binned your balls and you're out the gate. Thank you.
SPEAKER2: Thank you.
[ROUND 3]
HOST: [CUT] Louise didn't tell a lie all day. But for all that she now makes an early exit. Ten golden balls remain in play - eight cash balls and two killer balls. They are going back into the ball machine and Amanda, the girl we love to hate, and hate to love adds the final killer ball. But somewhere in those eleven balls we know are the five cash amounts that can make up your maximum potential win today. We've seen them already, they are one-thousand-four-hundred pounds. One-thousand-seven-hundred. Ten-thousand pounds. Thirteen-thousand pounds. And the fifteen-thousand pounds golden balls. (inaudible) But now you have to play together, to build up as much cash as you can. It's time to play “Bin or Win”. There we are. Final stages it is where you wanna be!
SPEAKER4: Yep.
SPEAKER3: Brilliant.
HOST: It was a tough call there wasn't it?
SPEAKER3: Yeah.
HOST: I mean, you were all three telling the truth.
SPEAKER4: Yeah.
HOST: And in the end, was it- was it down to sort of cash or…
SPEAKER3: Well, uh, I thought I look, I know I can trust her.
HOST: Yeah.
SPEAKER3: Like that’s how I feel.
SPEAKER4: Uhm, I felt like I could trust him in the last round, it remains to be seen when we does a bit of chat.
SPEAKER3: Yeah.
HOST: Right.
SPEAKER4: I mean, how much we're playing for, but uhm I'm I'm hoping I can trust him, yeah.
HOST: Alright. Well, I can tell you today you're playing for a jackpot of forty-one-thousand-one-hundred.
(clapping, speech inaudible)
HOST: Fifteen-thousand and thirteen-thousand pound balls into these golden five slots here. Let's see how we get on. We're going to play “Bin or Win”. You each take a turn to pick a ball to bin and then one to win. The ball to bin goes out of the game altogether, whether it's the fifteen-thousand pound ball or a killer ball. You have brought through two killer balls, we’ve added one, and there's three killer balls in those eleven golden balls there.
SPEAKER4: Yeah.
HOST: After picking a ball to pick, you then pick a ball to win. The ball to win goes into one of these top golden five slots here and the balls to win add up for your jackpot total towards the end of the game. While you're accumulating cash, if or when you pick a killer ball, it knocks a nought off your accumulated total. So if you've had that. Say a fifteen-thousand pound ball there, the ten-thousand pound ball there, that's twenty-five-thousand, and then you pick a killer. The twenty-five goes down to two-thousand-five-hundred.
SPEAKER4: Mhm-hm.
HOST: That's how damaging they are.
SPEAKER3: Yeah.
HOST: However, if you can get rid of the killer balls early on, then you're in for some very decent money today. Peter you brought through the most amount of money, so please kick us up by picking the ball to bin.
SPEAKER3: Right now I’ve noticed you were doing some nice deep breathing.-
SPEAKER4: - Uh-hu -
SPEAKER3: -So have you any feelings about these cause I …?
SPEAKER4: I I think it's it's uh random and I think you should just you should go with the ones that you think and I'll just go for the ones that -
SPEAKER3: - Right -
SPEAKER4: - I think then we can't blame each other.
SPEAKER3: Yeah, OK, that's lovely. I'll go for that one, alright.
HOST: Right, show Julie once you've chosen please Peter.
SPEAKER3: One-thousand-four-hundred.
SPEAKER4: Not too bad.
HOST: Not too bad. Needs to be binned. It's just affected your highest possible win, but it's still forty-thousand-eight-hundred pounds.
SPEAKER4: Still doing well.
HOST: You're doing very well. So pick up ball to win Peter.
SPEAKER3: Right. I'm gonna go exactly opposite.
HOST: I guess that one (inaudible )my job.
SPEAKER3: Just in time.
HOST: Rushing a little bit (inaudible)
HOST: First winning Golden Ball today is … one-thousand-seven-hundred pounds.
SPEAKER4: Okay.
HOST: It’s cash, uh, ot doesn't affect your highest possible win, it's still forty-thousand-eight-hundred pounds. Julie your turn.
SPEAKER4: Yeah.
HOST: Pick a ball to bin.
SPEAKER4: I'm gonna go for the middle one.
HOST: Ah, would be great if it’s a killer ball.
SPEAKER4: It would be fantastic.
HOST & SPEAKER4: It’s one-thousand-one-hundred.
HOST: It needs to be binned please Julie.
SPEAKER4: Okay.
HOST: One-thousand-one-hundred. Still three killer balls out there.
SPEAKER4: Yeah.
HOST: It's your choice to pick a ball to win. If you pick a killer ball at this stage, the seventeen-hundred pounds goes down to one-hundred-and-seventy.
SPEAKER4: Yeah.
SPEAKER4: I'm gonna go for this on here.
HOST: Okay. Happy?
SPEAKER3: Yeah, very happy.
HOST: The second winning golden ball today is … It’s a killer.
SPEAKER4: Oh.
SPEAKER3: Uhmm, asks -
SPEAKER4: - not too bad. Too bad
SPEAKER3: As you say, you get rid of it early it's not too bad at this stage, seventeen-hundred pounds, goes down to a-hundred-and-seventy. Your highest possible win has been affected, but not gravely. It's still thirty-eight-thousand-one-seventy.
SPEAKER4: Okay. We can still win this.
SPEAKER3: Yeah.
HOST: You've gotta get the fifteen and the thirteen-thousand balls into these slots if you can.
SPEAKER3: Right.
HOST: Back to you.
SPEAKER3: Yeah.
HOST: In the ball bin Peter.
SPEAKER3: Well, let's get up.
HOST: There's two killers out there. If this is one of them -
SPEAKER3: - This is one of them. It’s gotta be
HOST: I trust you.
SPEAKER4: Yes!
HOST: It’s a (inaudible)
SPEAKER3: Yes!
HOST: Good man Peter. Good man.
SPEAKER4: So there's -
HOST: - Only one more killer to find. Peter, pick a ball to win
SPEAKER3: Opposite end of the scale, and go there.
SPEAKER4 & SPEAKER3: (inaudible, overlap)
HOST: You're looking for a fifteen-thousand pound ball there, or a thirteen-thousand, -
SPEAKER3: - Yeah -
HOST: - or a ten-thousand. They're all on that table.
SPEAKER3: Yep.
HOST: Let's hope this is one of them. The third winning golden Ball today is … hmm … ten-thousand pounds. (inaudible) You now have ten-thousand -
SPEAKER4: - Yeah -
HOST: - one-hundred-and-seventy (inaudible) Alright.
SPEAKER4: I’m feeling the pressure now.
HOST: Julie.
SPEAKER4: Yes.
HOST: Has a situation, you're still going for thirty-eight-thousand-one-hundred-and-seventy. There is a killer lurking in-between those five balls. Pick a ball to bin.
SPEAKER3: Right. Just, come on.
HOST: No, it’s six-hundred-and-fifty. Not bad, really, not bad.
SPEAKER4: Okay.
HOST: That's to be binned.
HOST: Now, you really don't want the killer at this stage.
SPEAKER4: Hmm.
HOST: Pick a ball to win.
SPEAKER4: Hah, I’m counting out the letters of my children's name in my head. This one.
HOST: Your fourth winning Golden Ball today is ... Hatch! I've been bitten by the honesty book, honestly. We’re going to a break.
(break)
HOST: [CUT] Welcome back to Golden Balls! Before the break. Peter and Julie had accumulated ten-thousand-one-hundred-and-seventy pounds (inaudible) There’s four balls there. One of them a killer, but there's a thirteen-thousand pound ball and a fifteen-thousand pound ball. If this is one of the big ones, you're on your way to thirty-eight-thousand-one-hundred-and-seventy pounds. The fourth winning Golden Ball today is … Yes, yes, yes! Thirteen-thousand!
SPEAKER3 & SPEAKER4: (inaudible, overlapping speech)
HOST: Oh. You had ten-thousand-one-hundred-and-seventy. You now have twenty-three-thousand-one-seventy. Peter, back to you. Now here's the situation. Three golden balls left. That's one killer ball in amongst those three. There is a fifteen-thousand pound ball and there is a two-hundred pound ball. If you can pick the killer, you're on for thirty-eight-thousand-one-hundred-and-seventy.
SPEAKER4: Mhm-hmm.
HOST: No pressure, Peter.
SPEAKER4: Hmm.
SPEAKER3: Thank you.
HOST: Pick a ball to bin.
SPEAKER3: Well, two cash, one killer.
SPEAKER4: Yeah.
SPEAKER3: So two cash, one killer.
SPEAKER4: OK.
SPEAKER3: Which way, which way.
SPEAKER4: I’m fine with that.
HOST: Let’s see how long (inaudible)
SPEAKER4: Okay.
HOST: It’s gotta be a killer. Come on.
SPEAKER3: Let you see first, I don’t see -
SPEAKER4: Yeah!
HOST: Yes!
HOST: Great! Great, that's great, isn’t that -
SPEAKER4: - That is fantastic.
HOST: It’s cash, all the way.
SPEAKER4: Yeah.
HOST: However, there's still a little way to go because -
SPEAKER4: - yeah -
HOST: - there's two balls left.
SPEAKER4: Yeah.
HOST: One is worth fifteen-thousand pounds, one is worth two-hundred pounds. This is a fourteen-thousand-eight-hundred pound decision. Pick a ball to win.
SPEAKER3: Left the fifteen grand, didn’t ya?
SPEAKER4: I did, -
SPEAKER3: Nice one.
SPEAKER4: I did.
SPEAKER3: So should we say you've got it now and this one's not, so I’ll go -
SPEAKER4: - Yeah, an-
SPEAKER3: - to that one.
SPEAKER4: If you like, yeah.
SPEAKER3: Go for it. There you go. Whatever happens.
SPEAKER4: Yeah
HOST: Your fifth, and final winning golden ball today is … aww … fifteen-thousand!
SPEAKER3: Wohoo!
HOST: (inaudible)
SPEAKER4: Well then.
HOST: What's this?
SPEAKER3: Oh.
HOST: Two-hundred?
SPEAKER4: Bin it.
HOST: You bin it!
SPEAKER3: Ya get it.
SPEAKER4: (inaudible)
HOST: You've got a jackpot total of thirty-eight-thousand -
SPEAKER4: - yeah -
HOST: - one-hundred-and-seventy.
[SPLIT/STEAL]
HOST: Right. You've got it. Can you keep it? It's time to play split. Or steal. Julie, Peter. Two final golden balls in the game, the most important balls in the game, certainly in this one. You each have a golden ball which is worth split. Written inside you each have a golden ball with the word steal written inside. I will ask you to check your two golden balls to make sure you know which is the split ball, which is the steal ball. I will then ask you to make a conscious choice of picking the split or the steal ball. Neither of you will know what the other has chosen. If you both choose the split ball, you will split today's jackpot at thirty-eight-thousand-one-hundred-and-seventy and you will go home each with nineteen-thousand-and-eighty-five-hundred. If one of you chooses the split ball, one of you chooses the steal ball, whoever chooses the steal ball pockets the whole thirty-eight-thousand-one-hundred-and-seventy. Whoever chooses the split ball goes home with nothing. If you're both greedy and you both choose the steal ball, you two go home with nothing.
SPEAKER3: Serious?
HOST: Yeah. Very carefully check your two golden balls to make sure you know which is the split and which is the steal ball. Very careful. Before I ask you to choose, I give you a couple of minutes to talk about what your decision is going to be.
SPEAKER3: I- I really want both to go home with money.
SPEAKER4: We could actually take it all the way -
SPEAKER3: - Yeah -
SPEAKER4: - with no lies for three whole rounds, which would be fantastic.
SPEAKER3: It would be amazing.
SPEAKER4: And I'm hoping that the fact that you play Father Christmas and wouldn't be seeing on the streets with uh-
SPEAKER3: Wouldn’t be good, but please come here.
SPEAKER4: I'm gonna split.
SPEAKER3: Please.
SPEAKER4: OK, I'm gonna split. I am one-hundred percent. I guarantee, guarantee going to split.
SPEAKER3: OK.
SPEAKER4: Okay?
SPEAKER3: Going to split.
SPEAKER4: OK, well I've made- I've made my decision.
SPEAKER3: I made my decision.
HOST: Peter?
SPEAKER3: Yes.
HOST: Julie?
SPEAKER4: Yeah.
HOST: Choose the split or steal ball now. And hold it. Julie, are you happy with your decision?
SPEAKER4: I'm happy with my decision, yeah.
HOST: Are you happy Peter -
SPEAKER3: - yes -
HOST: - with your decision?
SPEAKER3: Yes, definitely.
HOST: Are you ready? Split or steal?
[END]
SPEAKER4: Yes!
HOST: Wohoo! Yay!
(all hug, speech inaudible)
SPEAKER3: (inaudible) I came here with the intention of stealing.
SPEAKER4: I knew you did.
SPEAKER3: And I thought no, I’m not gonna do that but I’m gonna split.
SPEAKER4: Thank you so much.
SPEAKER3: Wonderful!
SPEAKER4: I changed my mind (inaudible, both talking)
HOST: Right, would you please (inaudible) nineteenthousand-an-eighty-five pounds!
HOST: [CUT] They, (inaudible) and they totally deserve the money. They've been fantastic. (inaudible) next time, goodbye!
SPEAKER4: [CUT] When Peter revealed the split ball I was absolutely ecstatic. I haven't had to lie through the whole game and it just gave me a great, great feeling.
SPEAKER3: [CUT] Having one-o nineteen-thousand pounds I can go back, and Father Christmas has told the truth.
