[INTRO]

HOST: Hey, guess what?
HOST: Everyone on today's show is completely honest.

[CUT]

SPEAKER4: All I'm saying is, I can give you my word.
SPEAKER4: I've been honest throughout the whole game.
SPEAKER4: I'm an honest guy.

[CUT]

HOST: Everyone can be totally trusted.

[CUT]

SPEAKER1: If I'm lying now, okay, then in the next round, you can just kick me off straight away.

[CUT]

HOST: No one has a reason to lie.

[CUT]

SPEAKER2: I could never, ever steal in this game.
SPEAKER2: Never.

[CUT]

HOST: Well, there's three fibs to get us going.
HOST: I wonder how many more we're going to get today on Golden Balls.
HOST: Thank you very much.
HOST: Thank you.
HOST: Hi, and welcome to the show where you can win a fortune, but only if you've got the balls.
HOST: Yep, it's Golden Balls.
HOST: Once again, we have four players looking to bluff their way to the big money.
HOST: A managing director, a student, a publisher, and a group risk manager.
HOST: And we know now what it's all about, the Golden Balls.
HOST: And there's a hundred more just like this one behind me in the Golden Bank.
HOST: So let's get things rolling.
HOST: Start the ball machine.
HOST: Ah, the Golden Bank.
HOST: Stuffed full of riches.
HOST: Golden Balls, all filled with money.
HOST: We know one of them's worth ten pounds.
HOST: And we also know that one of them is worth seventy-five thousand pounds.
HOST: But how much is coming out to play today?
HOST: Who knows?
HOST: But one thing we know is that bad, blonde ball killer Amanda is there to put four of her killer balls in amongst them.
HOST: And so there they are, the first sixteen Golden Balls of today's game.
HOST: Cash and killers.
HOST: And as always, each player receives four balls at random.
HOST: That's two for the front row and two for the back.

[CUT]

[ROUND 1]

HOST:Good afternoon everybody.
HOST: Welcome to Golden Balls.
SPEAKER1: Hi, Jasper.
HOST: Four very straightforward, right down the line, honest people.
HOST: Yes?
All: Yes.
HOST: Ah, sixteen Golden Balls out there.
HOST: Four of them are killers.
HOST: Where are they?
HOST: We don't know.
HOST: Twelve of them contain cash.
HOST: How much?
HOST: We don't know.
HOST: We're about to find out.
HOST: Our first player today is Peter from Belfast.
HOST: Good afternoon Peter.
SPEAKER2: Thank you.
HOST: Welcome to the show.
HOST: Lovely smile.
HOST: What do you do?
SPEAKER2: I publish tour escape books.
HOST: For where?
SPEAKER2: For Ireland.
HOST: Because you're from Belfast in the name.
SPEAKER2: Exactly.
HOST: You played tennis for Ireland.
SPEAKER2: I did, when there was Andrea Teng.
HOST: Yeah?
SPEAKER2: So many years ago.
HOST: Ireland's not renowned for its tennis players.
HOST: I mean, I can't think of a famous Irish tennis player.
SPEAKER2: Neither can I. 
HOST: No, we're not much better off in this country.
HOST: I mean, I play tennis about six times a year here and I'm seventh ranked in England.
HOST: Now, you've got a lot of friends and they call you Lucky.
SPEAKER2: They do.
HOST: Why's that?
SPEAKER2: Well, in the last five years I have had three detached retinas, ruptured Achilles tendon and a prolapsed disc.
SPEAKER2: Other than that I'm very fit and healthy.
HOST: Peter, you're first up today.
HOST: Show us what's in your front two golden balls please.
SPEAKER2: My first ball is six hundred pounds.
SPEAKER2: Small amount but not a killer.
HOST: Not a killer.
SPEAKER2: And my second ball is twelve hundred pounds.
SPEAKER2: Double the amount.
HOST: No killers, twelve hundred pounds.
HOST: Lucky the Irish.
HOST: Our second player is Jane from Cheshire.
HOST: Hi Jane.
SPEAKER3: Hiya.
HOST: Lovely to have you on the show.
SPEAKER3: Thank you.
HOST: What do you do?
SPEAKER3: I'm a group risk manager.
HOST: Come on, explain.
SPEAKER3: It's just a really posh way of saying I deal with pensions and life assurance.
HOST: Oh, interesting.
SPEAKER3: No.
HOST: Oh, boring.
SPEAKER3: That's why I say I'm a group risk manager.
HOST: Oh, I see.
SPEAKER3: I'm racy and exciting.
HOST: You're getting married.
SPEAKER3: He actually proposed to me on Christmas Day and said, we'll get engaged but we can't afford to get married so we'll just have to be engaged.
SPEAKER3: So I said, right, blow it.
SPEAKER3: I'll go on golden balls and I'll win the money and you don't have an excuse.
HOST: Jane, what have you got on your front row?
SPEAKER3: Not the best of starts.
HOST: Okay, killer.
HOST: But there's four of them.
HOST: There's four of them.
HOST: They could be anywhere.
SPEAKER3: Okay, it's only a little but at least it's cash. 
HOST: Two hundred pounds.
SPEAKER3: Yeah.
HOST: Our third player is Matt from Suffolk.
HOST: Hi, Matt.
SPEAKER4: What do you do?
SPEAKER4: I'm management director of my own company.
HOST: Of?
SPEAKER4: It's advertising.
HOST: Yeah.
SPEAKER4: We advertise other companies' brands and reward our customers who use their brands for going through our site.
HOST: For loyalty?
SPEAKER4: Yeah, basically.
HOST: You reward them for loyalty because loyalty is one of your major strong points.
HOST: I mean, in the last twelve months you've only had twenty jobs.
SPEAKER4: Yeah, give or take a few. 
HOST: Twenty jobs in a year.
SPEAKER4: I'm getting bored of them, really.
HOST: I mean...
SPEAKER4: I've tried everything, really.
SPEAKER4: There's not really a job I haven't tried.
SPEAKER4: But now I work for myself.
SPEAKER4: That's what I want to do.
HOST: Okay, Matt.
HOST: What have you got on your front row?
HOST: Looking for three killers.
SPEAKER4: Well, there's one of them.
HOST: There you are.
HOST: I told you they could be anywhere.
HOST: And?
SPEAKER4: It's got to be somewhere.
SPEAKER4: The second one.
SPEAKER4: Not a bad amount.
SPEAKER4: Eight thousand five hundred
All: Whoa!
HOST: I wonder what our fourth player thinks about it all.
SPEAKER1: It's Helen from Streatham.
HOST: Hi, Helen.
SPEAKER1: Hi, Jasper.
HOST: Lovely to have you on the show.
HOST: What do you do?
SPEAKER1: I'm a media and music student.
HOST: Hoping to do what?
SPEAKER1: I want to become a PR, hopefully, when I finish my degree.
HOST: Now, interesting for the other three, because now be very careful what you say here.
HOST: You happened to find a bag just a short while ago with one hundred and sixty pounds in it.
SPEAKER1: Yes.
HOST: What did you do with it?
SPEAKER1:I kept it.
SPEAKER1: It was a little money bag.
HOST: Yeah.
SPEAKER1: That I found them the bus.
HOST: If it had been in a big money bag, you'd have handed it in.
HOST: It will not be held against you, will it?
HOST: They're as big a liar as anybody.
HOST: Helen, your last person up today.
HOST: Great fun.
HOST: What have you got on your front row, please?
SPEAKER1: Well, I've got a killer.
HOST: Killer?
HOST: Oh, I mean, they're even that today.
SPEAKER1: Not very much here as well. One hundred and seventy five.
HOST: Right.
HOST: We have eight golden balls on show, three killers on show, only one to find.
HOST: Not a lot of cash out there.
HOST: You each have two golden balls on the back.
HOST: If you don't know what they are, I'm going to ask you to have a look to see what they are.
HOST: Now, for your eyes only, don't show the other players what you have.
HOST: I'll come back and find out what you claim they've all got in them.
HOST: You all know what you have on the back rows We're going to play hunt the killer because we have three killers on show only one to find now You've got a killer on the front.
SPEAKER4: I have.
HOST: But you've got eight thousand five hundred Do you have any killers on the back. 
SPEAKER4: Got no killers.
SPEAKER4: No, but I've got ten thousand pound ball.
HOST: Yeah.
SPEAKER4: And a one thousand two hundred fifty pound bull 
HOST: Helen one missing killer!
HOST: Have you got it?
SPEAKER1: No.
HOST: What have you got?
SPEAKER1: This one's not very high It's only nine hundred and fifty pounds.
HOST: Okay.
SPEAKER1: But here I've got seven thousand five hundred. 
HOST: So nine minutes lifted and seven thousand five hundred.
HOST: Yeah Peter you are killer free on the front.
SPEAKER2: Absolutely.
HOST: Are you killer free on the back?
SPEAKER2: Absolutely.
SPEAKER2: Yep I have a twenty pound ball here and a five thousand pine ball here.
HOST: Jane.
HOST: Have you got any killers?
SPEAKER3: I haven't no no popular one.
SPEAKER3: It's on the front.
SPEAKER3: Yeah, and I've got five thousand five hundred here.
HOST: Yeah. 
SPEAKER3: And four thousand here. 
HOST: So! Any idea where the killer would be at this stage? 
SPEAKER4: Well, can I just say? 
HOST: Yes Matt?
SPEAKER4: This is honest.
SPEAKER4: There is ten thousand pounds there and one thousand two hundred and fifty there's no reason for me to lie with the biggest amount showing 
SPEAKER3: When I feel is our piece if Peter had a killer He could probably afford to say that he's got a killer 
SPEAKER2: Or give larger amounts.
HOST: I'm sorry. Helen? 
SPEAKER1: What these two numbers here quite low, but I've got seven thousand five hundred here  And I think that is quite a good amount of money to take throughs to the next round 
SPEAKER3: I'm worried about that could be the killer.
SPEAKER3: I just want to find out of a killer because the pain is the other killer. 
SPEAKER1: If I'm lying now, okay, then in the next round you can just kick me off straight away.
SPEAKER4: I know that's the thing.
SPEAKER3: We should all be truthful.
SPEAKER3: The thing is what we've got to look at is if Peter Had a killer he could afford to say he's got a killer 
SPEAKER1: Yeah.
SPEAKER3: So I'm inclined to believe Peter. The one person who I think is in the weakest position here is Helen.
SPEAKER1: I think you're in the weakest position because you've got two hundred right there and the killer showing there. 
SPEAKER3: My two hundred is too high?
SPEAKER3: I'm gonna go on.
SPEAKER1: What was your two?
SPEAKER3: five thousand five hundred.
SPEAKER1: Yeah 
HOST: Thank you Peter 
HOST: There is that intangible factor of trust.
SPEAKER2: Exactly. 
SPEAKER3: Before I even came on the show I would rather split and have the right conscience and know I did the right thing.
SPEAKER1: It's a bit strange how all of a sudden she's starting to attack me because she knows that you two 
SPEAKER4: We have to go on and trust Peter.
SPEAKER4: Who do you personally think who do you think's got the other killer?
SPEAKER2: Do you definitely not have the second?
SPEAKER4: I promise you that.
SPEAKER4: I'm not a killer and one thousand two hundred and fifty I promise you that and you'll see in the next round with the most on there.
SPEAKER4: I can afford to tell you if there's a killer.
SPEAKER1: Yeah.
SPEAKER1: I'm telling you probably I haven't got a killer.
SPEAKER1: So we've got to decide and you're gonna find out in the next round. 
SPEAKER1: that person is lying 
SPEAKER1: Who's James?
SPEAKER3: I'm telling you I've got the one killer nine fifty five thousand five hundred and fifty on there and I've got four thousand there.
SPEAKER3: I think Helen has And she's going... 
SPEAKER1: I you're the one that's got the killer! 
SPEAKER3: and you say she's got low amount so that you'll think oh She's saying she's got low amount 
SPEAKER1: I've said nine thousand five hundred, you know, because that's one part.
SPEAKER1: That's only a hundred and seventy five here 
SPEAKER1: But at the end of day, I feel that how I need to be honest seven thousand five hundred is in this game.
SPEAKER3: You know hundred percent split.
SPEAKER3: Even if it's fifty quid at the end of the game, I will split. 
SPEAKER2: And if it's a hundred thousand? 
SPEAKER3: If it is a hundred thousand I will split.
HOST: Now, Peter why should they keep you in the game? 
SPEAKER2: Because I don't have large cash amounts, but I have no killer balls. 
HOST: Jane? 
SPEAKER3: I've got no killer balls.
SPEAKER3: I've got a reasonable amount and they're split quite evenly as well between the two balls So if they went through you've got a good chance of winning and I'm going to split I honestly am 
HOST: Matt 
SPEAKER4: Well, I've obviously got the main balls in the game the highest amounts and I'm gonna split 
HOST: Helen 
SPEAKER1: If you keep me in the game You're not gonna take through with you two killers plus seven thousand five hundred nine hundred fifty and a hundred of seventy five pounds 
HOST: It's very finely poised three killers on show one to five.
HOST: Where's the missing killer?
HOST: Who's got what they say they've got?
HOST: It's all about trust.
HOST: Someone has to leave golden balls 
HOST: The four of you have to decide Who it's going to be 
HOST: And you have to decide now 
HOST: The players have decided which one of them is leaving the game of them is leaving the game.
HOST: They all seem so nice when I met them and then they go and ruin it by like oh you're lying, no you're lying, he's lying, she's lying, someone's lying 
HOST: Join me after the break, find out who it is 
HOST: Welcome back to golden balls, the votes are in 
HOST: Our four players have decided who is about to leave the game And see their golden balls binned 
HOST: Before the break our four players were searching for one missing killer ball 
HOST: They were all very convincing. 
HOST: But now it's time to reveal who's been chosen to leave. 
HOST: The first vote is for:
HOST: Jane. 
HOST: The second vote is for: 
HOST: Helen. 
HOST: The third vote is for: 
HOST: Jane. 
HOST: The fourth and final vote is for: 
HOST: Helen. 
HOST: It's a tied vote: two votes for Jane, two votes for Helen. 
HOST: In this situation the players that receive no votes That's Matt and Peter will discuss which one of either Helen or Jane is to leave Golden Balls. 
HOST: While they're discussing it, Helen, Jane you have no further part to playing golden balls until they've finished. 
HOST: Peter, Matt!
SPEAKER2: I'm going to come clean right at the start, I have a killer I only have eighteen hundred pounds in total so that's why. 
SPEAKER4: So it means go with money don't we?
SPEAKER2: Well, Jane has the most money and I hundred percent believe that Jane will split. 
SPEAKER4: Now you said you had that killer.
SPEAKER2: Yeah.
SPEAKER4: Yeah, is the money, it has to be Jane I think, I think I'll go with you on that 
SPEAKER2: Sorry Helen 
SPEAKER4: Sorry Helen 
HOST: What is your decision?
SPEAKER2: Jane to stay Jane to stay 
HOST: Matt, what is your decision?
SPEAKER4: Also, Jane to stay
HOST: Helen, sadly you have to leave golden balls
HOST: You were claiming nine hundred and fifty, is that there?
SPEAKER1: Yes. 
HOST: So it is.
HOST: You're claiming seven thousand five hundred. 
HOST: Is it thousand five hundred or is it something less?
SPEAKER1: thousand five hundred
HOST: Telling the truth 
HOST: Jane, you were claiming a four thousand pound ball, is that there?
SPEAKER3: It is indeed Jasper. 
HOST: What Peter and Matt want to know, is that five thousand five hundred or is it make believe?
SPEAKER3: It's not quite five thousand five hundred but it wasn't a killer and I was telling you the truth, I didn't have two killers 
HOST: Alright Peter, you admitted a killer in the discussion 
HOST: There you go and twenty pounds twenty pounds Is that there?
HOST: twenty pounds is not nice And Matt, twelve hundred and fifty, is that there?
HOST: Yes, that is her, yeah And what?
SPEAKER4: Well, slightly wrong 
HOST: What a massive lie, it's only twelve hundred pounds 
HOST: But what Peter and Jane want to know, you were claiming ten thousand pounds, your integrity is at stake here What is in that ball?
SPEAKER4: True to my word, ten thousand. 
HOST: Helen, close run thing, could have gone either way You've played a great game, but you have to leave golden balls, but before you do could you please bin your golden balls.
HOST: Okay, it's the killer, one seven five and nine hundred and fifty.
HOST: A lot of money going.
HOST: Helen, you've been good balls but you're out of the game.
HOST: Thanks for playing.

[ROUND 2]

HOST: She found lots more money but this time didn't get the chance to keep it and spend it.
HOST: We say goodbye and good luck to Helen.
HOST: The twelve remaining balls go back into the ball machine.
HOST: And the golden bank adds two more mystery cash balls.
HOST: And of course Amanda, the girl who should know better but doesn't, adds one of her killer balls.
HOST: So there's now more money in the game but there's also one more killer.
HOST: You now have fifteen golden balls in play, killers and cash.
HOST: Each of our players now receives five balls at random.
HOST: That's two for the front row and three for the back.
HOST: Congratulations, you made it through what was quite a difficult round.
HOST: And I think there's another difficult round coming up because inevitably you have to bring through three killer balls.
HOST: There's no way out of that.
HOST: We've added one so there's four killer balls to find.
HOST: You have to find them and eliminate them because you know the damage they do to your jackpot.
HOST: However, I can tell you that the total that you bought through from the first round was twenty six thousand seven hundred and fifty pounds.
HOST: That's including the eigh thousand five hundred pound ball and the ten thousand pound ball.
HOST: Jane, close call.
HOST: Your fiancé was thinking, yes!
HOST: Damn it!
HOST: Peter, now we know why you're called lucky.
HOST: And Matt, we're obviously not boring the pants off you because you're still here.
HOST: While we've got you, what have you got on your front row this time?
SPEAKER4: Not bad, eight thousand five hundred pounds.
HOST: And?
SPEAKER4: Not bad again, twelve hundred pound.
HOST: Peter, what have you got on your front row?
HOST: Looking for four killers, remember?
HOST: six hundred pounds?
SPEAKER2: six hundred pounds.
SPEAKER2: Come back to me.
SPEAKER2: And three thousand five hundred pounds.
HOST: Jane?
SPEAKER3: twenty pounds.
HOST: Not a killer?
SPEAKER3: Not a killer.
SPEAKER3: Oh, good grief!
HOST: What?
HOST: seventy five thousand pounds?
HOST: Your fiancé spitting blood?
HOST: I've seen that in you both.
HOST: Hmm, could be, could be.
HOST: That is remarkable.
HOST: Six golden balls on show.
HOST: An enormous amount of money, but no killers.
HOST: Somewhere on the back row of balls, four killers are hiding.
HOST: You each have three golden balls on the back.
HOST: You don't know what they are.
HOST: I'm going to ask you to look to find out what they are, and we're going to come back and see what you're claiming.
HOST: Peter, Jane, Matt, have a look at your back row of balls now.
HOST: Wow!
HOST: This is a very, very tricky round here.
HOST: This is all about trust.
HOST: There's nine golden balls on the back rows, four of them are killers.
HOST: What is guaranteed is that someone has at least two killer balls.
HOST: As you can see, there's a load of money on the front, over eighty odd thousand.
HOST: Oh, wow!
HOST: Jane, seventy five thousand.
HOST: Out of the blue, fantastic.
HOST: What have you got on your back row?
SPEAKER3: OK, straight away, Jessica, that is a killer ball.
SPEAKER3: That one's four thousand, definitely, and I'm really sorry, because I got so excited by that one, so...
HOST: Have a quick look, just keep it to yourself. ...five hundred and fifty.
HOST: So that's five fifty, four thousand, and a killer.
SPEAKER3: And I've got one killer ball.
HOST: Peter!
SPEAKER2: I will also admit I have one killer ball.
SPEAKER2: I have the ten thousand that Matt had in the last round, and I have the one thousand two hundred that I had in my own front row the last time.
SPEAKER4: Well, I know there's a lie already.
SPEAKER4: I'm going to admit to a killer, but I've got ten thousand and two hundred.
SPEAKER4: So I know straight away you're lying.
SPEAKER2: That is hundred percent the ten thousand ball.
SPEAKER4: And from a previous experience, you lied before.
SPEAKER2: I admit I lied before.
SPEAKER2: I had to lie before because I had only a twenty five -ball and a killer.
SPEAKER4: Well, I think the main thing is getting all the money through.
SPEAKER4: Either way, you're going to have to take a killer for it.
SPEAKER3: Matt, I'm really sorry, but it's just something about the way you've changed all your jobs and everything.
SPEAKER3: Why would I believe that I can trust you?
SPEAKER4: Well, all I can say is going from the last round out of everyone, I was the one that was honest about all balls, and I'm being honest now.
SPEAKER3: So the main thing for me is to take someone through.
SPEAKER3: I'd rather win less money and go fifty fifty.
SPEAKER3: Because if I take through a whole load of money, and then whoever I take through steals it, I'm going to go home with nothing, and I'm going to have given somebody else loads of money.
SPEAKER4: Look what we've got going through, seventy five thousand, the ten thousand, eigh thousand five hundred.
SPEAKER4: We've got almost one hundred thousand pounds.
SPEAKER4: That's fifty thousand pounds each.
SPEAKER4: That's a lot of money.
SPEAKER4: If you're taking Peter through, you're taking two killers through, which is going to knock your seventy five thousand out.
SPEAKER4: That's seven hundred fifty pounds.
SPEAKER4: All I'm saying is I can give you my word.
SPEAKER4: I've been honest throughout the whole game.
SPEAKER3: I think that's OK, so you've been honest because you've got lucky balls.
SPEAKER2: Exactly.
SPEAKER3: I think if you'd have had a killer, I think you'd have lied.
SPEAKER3: Like I told a little white lie.
SPEAKER4: I've been completely honest with you.
SPEAKER2: I'm going to split.
SPEAKER4: I believe you're going to split.
SPEAKER2: In terms of the first two rounds, I have no difficulty bluffing.
SPEAKER2: In terms of the third round, I could never, ever steal in this game.
SPEAKER2: Never.
SPEAKER4: But I think if you're going to lie now, then what's at the end?
SPEAKER4: Because at the end, it's real money.
SPEAKER2: Jane, as you know, I took you through in the last round because I believed hundred percent that you would split, and I totally know that irrespective of whether you had the seventy five thousand pounds or not.
SPEAKER4: But then two killers, that's going to knock the money right down.
SPEAKER2: It's only one killer.
SPEAKER2: It's a ten thousand pound ball.
SPEAKER4: I don't believe that.
SPEAKER4: Unless Jane's lying.
SPEAKER3: Hang on, if I had both killers, I'd say I've got one killer, five hundred fifty pound and the four thousand pounds I actually had in the first round.
SPEAKER2: I totally believe Jane.
SPEAKER2: I don't doubt Jane for one moment.
SPEAKER2: I know that that's a ten thousand pound ball.
SPEAKER2: The two thousand five hundred pounds.
SPEAKER2: The two balls out there, the seventy five thousand pound ball and the two thousand five hundred pound, and I know that's a ten thousand pounds.
SPEAKER4: Well, it can't be a ten thousand pounds if this is a ten thousand pounds and we've got the two new amounts.
SPEAKER4: What is the other new amount?
SPEAKER4: Let's work this out.
SPEAKER2: seventy five thousand pounds.
SPEAKER2: So the seventy five thousand pounds and the two thousand five hundred pounds are two new amounts.
SPEAKER4: So there can't be another ten thousand pounds, so I must be lying.
SPEAKER2: Correct.
SPEAKER4: So you're lying.
SPEAKER2: That is a ten thousand pounds pound ball.
SPEAKER4: All I can say, Jane, is I've been honest in both rounds.
SPEAKER3: I've never had two men fight over me before.
SPEAKER4: I've got a split.
SPEAKER3: It's the first time ever.
SPEAKER4: I'm not lying to you, Jane.
SPEAKER4: I mean, hundred percent I can guarantee she has two killers.
SPEAKER2: I have the ten thousand pounds pound ball, I promise you.
HOST: Matt.
SPEAKER2: Yes.
HOST: Why should they keep you in the game?
HOST: This is a really, really serious question.
SPEAKER4: It's very important.
SPEAKER4: I've got the next most amount of money, so you're going to have more money putting me through than Peter.
SPEAKER4: I can guarantee I'll split to you.
SPEAKER4: So if I went home with nothing, I mean, I'd be devastated.
HOST: Peter, why should they keep you in the game?
SPEAKER2: I will hundred percent split.
SPEAKER2: I would rather go home with nothing than steal from somebody.
HOST: Jane, it's pretty obvious why you... 
SPEAKER3: seventy five thousand reasons, yeah.
HOST: We have three admitted killers, one to find.
HOST: A lot of cash going through.
HOST: Someone is not going to be as lucky as they were in the first round because they will be leaving golden balls, and their golden balls will be binned.
HOST: The three of you have to decide who's leaving now.
HOST: Thank you, Amanda.
HOST: The votes are in.
HOST: Our players have decided.
HOST: Jane says she's never had two men fight over her before, but then maybe she's never had seven five thousand pounds before.
HOST: Peter and Matt have done their best to woo her, but who's won her heart?
HOST: We'll find out after the break.
HOST: Welcome back to Golden Balls.
HOST: Jane has got seventy five thousand pounds on her front row.
HOST: There are four killers hiding on the back rows.
HOST: Both Peter and Matt are claiming they have ten thousand pounds on their back rows.
HOST: The crucial question is, who's lying?
HOST: It's time to reveal who's survived and who's been binned.
HOST: The first vote is for...
HOST: Peter.
HOST: The second vote is for...
HOST: Matt.
HOST: The third and final vote is for...
HOST: Matt.
HOST: Close call.
HOST: Matt, Peter, you both were claiming the ten thousand pound ball.
HOST: Jane is dying to know who is telling the truth.
SPEAKER4: She'll be sorry.
HOST: You admitted a killer, Matt.
SPEAKER4: Yes.
HOST: Let's have a look at that.
SPEAKER4: There you go.
HOST: You claimed two hundred pounds.
SPEAKER4: Yep, that's there as well.
HOST: Jane, what do you reckon?
SPEAKER3: I don't know.
HOST: Matt, put her out of her misery.
HOST: Is it ten thousand pounds or...
SPEAKER4: It's a killer.
HOST: That's why.
SPEAKER4: Well done.
HOST: I honestly couldn't tell.
HOST: I really didn't know.
SPEAKER2: I knew.
HOST: Oh, lucky you.
SPEAKER2: Yes.
HOST: Peter, you admitted a killer.
HOST: And... twelve hundred pounds.
SPEAKER2: That was from the first round.
HOST: And of course, the ten thousand pounds.
HOST: There you go.
HOST: And Jane, the killer.
SPEAKER3: That's my killer.
SPEAKER3: There he is.
HOST: 550.
SPEAKER3: Middle in the middle.
HOST: And the four thousand pounds...
SPEAKER3: Is here.
HOST: Yeah, there you are.
HOST: Matt, sadly, you won't be taking part.
HOST: There was a lot of money there.
HOST: We had seventy five thousand coming up.
HOST: Great play, Matt.
HOST: You played a really great game.
HOST: But you have to bin your balls before you go.
SPEAKER4: That's a big amount. 
HOST: one thousand one thousand killer, killer.
HOST: two hundred.
HOST: Bin them.
HOST: one two three four.
HOST: Final one.
HOST: Matt, you binned your balls but you're out of the game.
HOST: Thanks for playing.

[ROUND 3]

HOST: Well, Matt lasted longer here than he's done in most of his jobs.
HOST: But now he's had to leave. ten golden balls remain in play. eight cash balls and two killer balls.
HOST: They're going back into the ball machine.
HOST: And look out because Amanda's on the prowl, adding one final killer ball.
HOST: But somewhere in those eleven balls we know are the five cash amounts that can make up your maximum potential win today.
HOST: We've seen them already.
HOST: They are... twelve hundred pounds.
HOST: two thousand five hundred.
HOST: four- and ten thousand pounds.
HOST: And that magnificent seventy five thousand pounds golden balls.
HOST: Peter and Jane, you've seen off the competition.
HOST: But now you have to play together to build up as much cash as you can.

[CUT]

HOST: It's time to play.
HOST: Bin or win.
HOST: Peter, Jane, congratulations.
HOST: You've made it through to the final stage.
HOST: Wow.
HOST: I mean, Jane, it's the vagaries of golden balls.
HOST: I mean, in the first round you were almost dead, out, gone.
HOST: Then in the second round the balls come your way and suddenly you're in control and you can decide who goes and who stays.
HOST: Peter, she brought you through and she was right.
SPEAKER2: I was a lot less a pointer.
HOST: Matt did have the extra killer ball.
HOST: So here we are.
HOST: Now, the good news is your maximum potential win today is... ninety two thousand seven hundred pounds.
HOST: That's what we want to see in the top golden prize.
HOST: You have eleven golden balls in front of you.
HOST: We're going to play bin or win.
HOST: What happens is that someone picks a ball to bin and whatever you pick goes out of the game.
HOST: Whether it's that seventy five thousand pound ball or the killer ball, it goes out of the game.
HOST: You then pick a ball to win.
HOST: The ball to win goes into this slot here.
HOST: You then pick a ball to bin, that goes out of the game, and another ball to win and that goes here.
HOST: Now, supposing you have picked the seventy five thousand pound ball, yes, and then the ten thousand pound ball, you've accumulated eighty five thousand pounds.
HOST: If you then pick a ball to win and it's a killer and, say, it goes here, it knocks a nought off your accumulated total.
HOST: So the eighty five thousand pound goes down to eight thousand five hundred.
HOST: And every time you pick a killer ball, it knocks a nought off your total.
HOST: That's why you've been trying to eliminate them.
HOST: Are you clear?
SPEAKER2: Yes.
SPEAKER3: Yes.
HOST: Jane, obviously you've bought through the most money.
HOST: You start by picking a ball to bin.
SPEAKER2: Go with whatever.
SPEAKER2: Total luck.
HOST: What you're trying to avoid is the seventy five thousand pound ball, but you're trying to pick one of those three killers which are out there.
SPEAKER3: I'll just go with the nearest one to me and just cross my fingers.
SPEAKER2: Okay, no problem.
SPEAKER2: Don't worry.
SPEAKER2: Whatever it is.
SPEAKER3: Please, please, please...
SPEAKER2: don't worry.
SPEAKER3: Oh!
HOST: What a start!
SPEAKER2: What a start!
HOST: Your fiancé's getting worried now, isn't he?
SPEAKER3: He's getting seriously worried now, yeah.
HOST: It has to be binned.
SPEAKER3: Goodbye.
HOST: Jane.
SPEAKER3: Okay.
HOST: Pick a ball to win.
SPEAKER3: Pick that one.
SPEAKER2: Fine.
HOST: What did they call you?
SPEAKER2: Lucky.
HOST: Your first winning golden ball today is... six hundred pounds.
SPEAKER2: Thank you.
HOST: You're up to a running start with cash.
HOST: Peter, pick a ball to bin.
SPEAKER2: Jane, I'm going to do the same as you, and I'm going to go with the ball nearest to me.
SPEAKER2: Sorry.
HOST: Oh, ten thousand pounds.
HOST: Has to be binned, but it's okay.
HOST: SPEAKER2: Sorry, Jane.
HOST: The seventy five thousand pound ball is still out there.
HOST: Two killers, seventy five thousand pounds.
HOST: Pick the seventy five thousand and you're on for a cash total of eighty three thousand three hundred pounds.
HOST: Pick a ball to win.
SPEAKER2: You'd be more lucky than me, Jane.
SPEAKER3: Whatever you think, just go for it.
SPEAKER2: That's a killer.
HOST: This is a killer, 
SPEAKER2: hopefully.
HOST: Is that what you're hoping for?
SPEAKER2: Yeah.
SPEAKER2: We're hoping for a killer now.
HOST: All right, well.
SPEAKER3: Quite cool with that.
SPEAKER3: That's fine.
HOST: Your second winning golden ball today is... twelove hundred pounds.
HOST: You did have six hundred pounds.
HOST: You now have one thousand eight hundred pounds.
HOST: But still two killers out there to find, and that seventy five thousan ball.
HOST: Jane, pick a ball to bin.
SPEAKER3: It worked last time, didn't it?
SPEAKER3: Just going for the ball nearest me.
SPEAKER3: So I'll go for this one to bin.
SPEAKER3: Yeah?
SPEAKER2: Yes, that's fine.
SPEAKER2: Yes!
HOST: Oh!
HOST: That's fantastic.
HOST: You have the golden touch, young lady.
HOST: The golden touch.
HOST: The six golden balls left.
HOST: One killer.
SPEAKER2: One killer.
HOST: The seventy five thousand pound ball is still out there.
HOST: Pick it now and you're on for a cash total of eighty three thousand three hundred pounds pounds.
HOST: Pick a ball to win, Jane.
SPEAKER3: OK.
SPEAKER3: I was drawn to this one, so I'm going to stick with it.
SPEAKER3: Even if it's a killer, it doesn't really matter.
SPEAKER2: A killer would be great.
SPEAKER3: Get rid of it.
SPEAKER2: Yeah, that would be fine.
SPEAKER2: Yeah, that's a bit, yeah.
HOST: The third winning golden ball today is...
HOST: Jane, your fiancé's on the phone.
HOST: He wants a word.
HOST: We'll be back when she's finished.

[CUT]

HOST: Welcome back to Golden Balls.
HOST: Before the break, Peter and Jane had accumulated eighteen hundred towards their Golden Five jackpot for today.
HOST: There are six balls out there, only one killer, and there's a seventy five thousand pound ball.
HOST: You're on for a jackpot total of eighty three thousand three hundred pounds, providing you can get the seventy five thousand pound ball from one of these blocks.
HOST: Your third winning Golden Ball today is twenty pounds.
HOST: You did have eighteen hundred pounds, it's now eighteen hundred and twenty pounds.
HOST: The seventy five thousand pound ball is still out there, so is the killer.
HOST: Peter, pick a ball to bin.
SPEAKER2: I'll tell you after I've already sold a one.
HOST: Oh, four thousand pounds after the bin.
HOST: Peter, pick a ball to win.
HOST: What a great time to pick that seventy five.
SPEAKER2: I'll go for that.
HOST: Your fourth winning Golden Ball today is twenty five hundred pounds.
HOST: You had eighteen hundred and twenty, now have four thousand three hundred and twenty.
HOST: Three Golden Balls left.
HOST: Peter, Jane, pick a ball to bin.
SPEAKER3: If I pick it now, Peter, I'm so sorry.
SPEAKER2: Don't worry, whatever.
SPEAKER3: One in the middle, that's to bin.
SPEAKER3: No blame on either side.
HOST: I don't know about you, but I'm going home.
HOST: If you pick the right ball, you're going for seventy nine thousand three hundred and twenty pounds.
HOST: Pick the killer and you're fighting over four three two pounds.
HOST: It doesn't come much more tense than this.
HOST: Jane, pick a ball to win.
SPEAKER2: What are you thinking?
SPEAKER3: I'm thinking that your nickname is Lucky.
SPEAKER2: I was thinking that ball too.
SPEAKER3: Both hands.
SPEAKER3: Both Jasper.
HOST: Let me put mine on as well.
HOST: Your fifth and final Golden Ball today.
HOST: It's the difference between four three two pounds and seventy nine thousand three hundred and twenty pounds.
HOST: Here it comes.
SPEAKER2: I'm sorry.
HOST: I don't believe it.
HOST: That is wretched luck.
HOST: I don't believe it. seventy five thousand pounds.
HOST: That is unbelievable.
HOST: Unbelievable.
HOST: Jane, bin it.
HOST: Not to be.
HOST: Not to be.
HOST: However, four three two pounds left.
HOST: If you split, you go home with two-hundred and sixteen pounds.
HOST: Not bad.
HOST: You have four three two pounds.
HOST: The question is, can you keep it?

[SPLIT/STEAL]

HOST: It's time to play split or steal.
HOST: You have two Golden Balls left in front of you.
HOST: They're not cash, they're not killers.
HOST: You each have a Golden Ball with the word split written inside.
HOST: You each have a Golden Ball with the word steal written inside.
HOST: I will ask you to check your Golden Balls to make sure which is the split ball and which is the steal ball.
HOST: And then you will make a conscious choice of choosing either the split or the steal ball.
HOST: Neither of you will know what the other has chosen.
HOST: If you both choose the split ball, you will split today's jackpot of four three two pounds and go home with two sixteen pounds.
HOST: If one of you chooses split and one of you chooses steal, whoever chooses the steal ball goes home with all the four three two pounds.
HOST: Whoever chooses the split ball goes home with nothing.
HOST: If you both choose the steal ball and try to be a little bit greedy, you both go home with nothing.
HOST: Check your two Golden Balls.
HOST: See which is the split ball and which is the steal.
HOST: Don't show each other.
HOST: Before you choose, just have a quick word with each other.
SPEAKER3: I said right from the start, I'm going to split.
SPEAKER3: No matter what, even if that seventy five thousand pound ball had been there, I was going to split.
SPEAKER2: I'm the exact same.
SPEAKER2: There is no way I would steal regardless of whether I've been a pound or a hundred thousand.
SPEAKER2: I'm hundred percent going to split.
SPEAKER3: Do you promise me?
SPEAKER2: I promise you.
SPEAKER2: I will hundred percent split.
HOST: Okay, choose split or steal now.
HOST: Are you ready?
HOST: Peter, Jane, split or steal?
HOST: Fantastic.

[END]

HOST: Well, it has been a superb game and I honestly believe you were both going to split and I think there wasn't a person who's watching or in the audience that didn't believe you would do it.
HOST: Fantastic.
HOST: Thank you for a great game.
HOST: There we go.
HOST: A perfect match made on Golden Balls.
HOST: They've been delightful.
HOST: This has been Golden Balls.
HOST: Until next time, goodbye.
SPEAKER2: When Jane revealed the split ball, I was delighted.
SPEAKER2: I had no doubt that she was going to split.
SPEAKER3: Golden Balls has actually taught me to go with your instincts and go with the person you trust.
SPEAKER3: You might not win as much, but at least you'll win something.

