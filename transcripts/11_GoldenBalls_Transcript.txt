HOST: [CUT] You can be good 
SPEAKER2: I am telling the honest truth, I'm all golden my balls are all golden 
HOST: [CUT] hey You can be bad 
SPEAKER3: if I had a killer ball then I would put my hand up and say do you know what I've got a killer ball
HOST: [CUT]You can even be downright ugly 
SPEAKER1: you throw these balls away. That's gone forever.
HOST: [CUT] You can do whatever it takes to be a winner on golden balls
HOST: Hi and welcome to golden balls four players all looking to talk their way to the big money a finance manager a
student an administrator and a hotel duty manager but who's got what it takes to take the lot and here's what it's all about the golden balls and behind me we have a hundred more of them in the golden bank 100 golden balls and they're all bursting with money from ten pounds right up to an amazing seventy-five thousand pounds. It's all they're just waiting to be won Just how generous will the golden bank be today that depends on which twelve balls it selects for our four players so let's find out start the ball machine
Here they are Let's say hello to the money a dozen golden balls of riches rolling their way towards today's players It's money all the way at the moment So we'd better put a stop to that and stick some killers into the mix and the ball's assistant Amanda adds four of these killers to the ball machine So that makes sixteen golden balls in the ball machine the wonderful cash and the woeful killers each player receives four at random two for the back row and two for the front
HOST: Right, how is everybody? Okay sixteen balls out there you've got four each
twelve of them are cash balls. We don't know how much is in them four of them are killer balls So whoever you think has got the four worst balls They've got to go and their golden balls will be binned and out of the game for good It's time to meet our first contestant today who is Emma from Chatham

[ROUND 1]

HOST: Hi Emma 
SPEAKER3: Hiyah are you alright 
HOST: I'm very well welcome to the show is Chatham still the naval town down there. 
SPEAKER3: Yeah
HOST: Yeah, and you do you know a few of the sailors?
SPEAKER3: No
HOST: No You're happily married of course
SPEAKER3: Oh no
HOST: What do you do for a living? 
SPEAKER3: Oh, I've a lift engineering company. 
HOST: Yeah, do you know what? Do you know what I think would be great for the lift companies. 
SPEAKER3: What's that?
HOST: a fart detector When the lift comes down and the doors open a big red light goes on you think I ain't getting in there Let's see what you got on your front row there
SPEAKER3: twenty thousand pounds
HOST: What have you got back in it?
SPEAKER 3: two hundred and fifty
HOST: all right Not twenty thousand, but my word. That's a really good ball. 
SPEAKER 3:That's a very good ball 
HOST: That's a great ball Our next contestant is Hugh from County Antrim
Welcome to you. 
SPEAKER 4:Thank you. 
HOST: And what do you do? 
SPEAKER4: I work in a hotel 
HOST: as? 
SPEAKER4: a junior manager I work out and look after the bar as well. 
HOST: Well, would that be in Belfast?
SPEAKER4: It is it's very close to one of the airports at Belfast
HOST: Do you know that you rope a hotel 
SPEAKER4: very well it actually has a claim as the most bombed hotel in Europe 
HOST: was [OVERLAP]
SPEAKER4: was was
HOST: Bombed hotel in Europe In fact when I stayed there I went in and the assistant was lovely
I can't really do a great Northern Ireland accent, but she's oh, Mr. Kirk
Welcome to the Europa hotel. Tell me she said, how did you hear about us?
I said well news at 10
Okay, we've seen what Emma's got. Can we see what you have on your front row?
SPEAKER4: So first one I have
seven thousand five hundred and
I'm gonna back that up with another thousand pounds. No killer ball 
HOST: no killers 
SPEAKER4: no killers
HOST: [CUT] Our next contestant is Helen from Watford Hi Helen
SPEAKER1: Hi 
HOST: Welcome to Golden Balls. 
SPEAKER1: Thank you. 
HOST: You work for your family haulage concern. Is that correct?
SPEAKER1: Yes, it's a family business. I run the finance section of it. 
HOST: All right
I mean do you get get stuck in with them big lorry drivers or you're the shrinking violet?
SPEAKER1: No, I get stuck in with the big fat hairy lorry driving type 
HOST: or the effing and blind 
SPEAKER1: yeah [OVERLAP]
HOST:you ever grind with them
SPEAKER1:I can give as good as I get 
HOST: you haven't got a tattoo of Popeye on your backside.
SPEAKER1: I'm not gonna show you
HOST:hahaha oh god no, Helen we need to see what you have on your front row. 
SPEAKER1:Okay, well, Only a small amount
two hundred 
HOST: but not a killer 
SPEAKER1: but not a killer and oh
I have a killer, but it's there for all to see.
HOST: [CUT] and our final guest is Owen from Pembroke.
SPEAKER2: Nice to meet you Jasper.
HOST: Nice to meet you Owen. What do you do? 
SPEAKER2:I'm a student in Cardiff University, and I do business with French. 
HOST: Why is it that we don't like the French? Do, do, do the French like us? 
SPEAKER2: Nah
HOST: Nah?
SPEAKER2: Nah, they don't like us. I was living in Paris when I was in France and the Parisians don't like anyone. They don't like the French.
HOST: Parisians the most miserable baskets on God's earth. You have to be a donkey to understand them [GIBBERISH] Have a nice day [SOUND EFFECTS]
Okay, Owen, can we see what you have on your front row open them up for us please 
SPEAKER2: Alright, here we go, first ball a hundred pound
HOST: but not a killer
SPEAKER2: Second ball, 650 pound
HOSt: Interesting. we now have eight balls on show, but only one killer ball.
There are three killer balls still to be revealed, but that's only half the story. Hugh, Emma,
Helen, Owen,
Look at your back balls,
now.
SPEAKER3: [STATEMENT OFF CAMERA] Generally, I am an honest person, so I'm gonna play honestly just to get through.
SPEAKER4: [STATEMENT OFF CAMERA] I'm gonna play an honest game, but I'm gonna lie through my teeth if I have to to get to that cat
SPEAKER1: [STATEMENT OFF CAMERA] I want to get the people to believe me implicitly. I need to get through. It's the only way to get to the final.
SPEAKER2: [STATEMENT OFF CAMERA] I'm here to win. I'm gonna string them along and then I'm gonna shoot them down.
HOST: Shortly you're going to have to vote off one of your opponents and their golden balls will be binned and out of the game for good.
We have one killer showing. you need to find out if you can where the other three killers are,
Because you know the damage they will do to your jackpot in the final stages.
Owen you don't have a lot of money showing on your front row. 
SPEAKER2: No
HOST: What have you got on your back row? 
SPEAKER2: I can guarantee to the other guys here that I have no killer balls.
I have a one thousand and a five hundred so they're not large totals, but there's no killer balls there
So I'm a safe bet for the next round. 
HOST: All right.
Helen you have the only killer ball showing 
SPEAKER1: I have and it is my only killer ball. 
HOST: Yeah? 
SPEAKER1: so I'm not too bad
I actually have one high amount of money
HOST: which is? 
SPEAKER1: this one forty-five thousand 
HOST: You have a forty-five thousand pound ball? 
SPEAKER1: [overlap] Yeah
HOST: that's terrific. 
SPEAKER1: This one is only twelve hundred, but it's money
That is my killer. You can see it. It's there for all to see
HOST: So if you, and Owen are telling the truth the three killers are over there. 
SPEAKER1: jap
HOST: that's what you're saying?
SPEAKER1: must be
HOST: Hugh do you own up to having any killers? 
SPEAKER4: It's funny that where the biggest amount of money is all of a sudden has to have the killers
I don't think so. I have eleven hundred pounds here six hundred pounds here. Yeah, but no killer balls. 
HOST: All right, so.
Emma, do you have any killers? 
SPEAKER3: I have no killers whatsoever
HOST: Mmm. Well, I'm sure you're all telling the truth. What have you got Emma? 
SPEAKER3: I've got eighty pounds and I've got five hundred pounds
HOST: eighty and five hundred 
SPEAKER3: [OVERLAP] yeah, again
HOST: but you have that twenty thousand at the front. 
SPEAKER3: I had that twenty thousand at the front 
HOST: All right. Owen. Who's telling lies?
SPEAKER2: Everyone
HOST: But not you 
SPEAKER2: no, I am telling the honest truth. I'm all golden, my balls are all golden,
I have no killer balls whatsoever 
SPEAKER2: Helen you're looking pretty unfortunate at the front there. 
SPEAKER1: [OVERLAP] I am upfront 
SPEAKER2: two hundred and a killer. 
SPEAKER1: [OVERLAP] My money is here. 
SPEAKER2: and of course You're gonna say you're gonna have a big amount at the back to 
SPEAKER1: [OVERLAP] naturally 
SPEAKER2: try and keep us on your side
SPEAKER1: [OVERLAP] Yeah, naturally but you've also
SPEAKER2:  [OVERLAP] but you got a grand to use the forty five grand just to get you through to the next round
SPEAKER1: [OVERLAP] Why?
SPEAKER2: I don't believe you. 
SPEAKER1: [OVERLAP] You've used two low amounts to get you through 
SPEAKER2: [OVERLAP] I've used low amounts
SPEAKER1: [OVERLAP] You probably have you probably have two killers. 
SPEAKER2: [OVERLAP] Nah, Helen. 
SPEAKER1: You're saying I've gone high to get me through
I'm saying you've gone low to get you through. 
SPEAKER2: [OVERLAP] Helen, You've got
SPEAKER1: [OVERLAP] You've got to bear in mind if I am telling the truth, which I am
You throw these balls away. That's gone forever. You will not get that forty-five thousand back
I am telling you the truth. 
SPEAKER3: If you have got another killer ball, please tell me. 
SPEAKER1: I have no other killer ball, Emma
SPEAKER3: I'll take you through. 
SPEAKER1: [OVERLAP]I have no other killer ball, Emma.
[HARD TO DISINGUISH DUE TO OVERLAP] 
HOST: Hugh
SPEAKER4: Yeah 
HOST: where are the killer balls? 
SPEAKER4: I know I haven't got any killers. So that means that there's, Three others among the three people there. 
SPEAKER2: So Hugh if I know I haven't got any killer balls, That leaves Helen and Emma 
SPEAKER3: if I had a killer ball, then I would put my hand up and say, do you know what?
I've got a killer ball, but I've also got twenty thousand pounds to match it, but I haven't 
SPEAKER2: now I've admitted to you guys that I've got low amounts
I have low amounts, but I have no killer balls. As I said I have a thousand and a five hundred
Add that you bring two grand through for the next round. It's a solid bet, especially with no killer balls. 
HOST: Hugh
Why should they keep you in golden balls? 
SPEAKER4: Well, it's quite obvious with the money
We have on the table in front of me. No killer balls into the next round is what we're looking for
I don't have a killer ball. So it's obvious that I should go through to the next round
HOST: Helen why should they keep you 
SPEAKER1: I have forty-five thousand pounds to my left and
twelve thousand pounds to my right my killer is there for you to see 
HOST: Owen. 
SPEAKER2: I'm an honest player
I should stand good stead for the next rounds. I have no killer balls. I have pure cash
You'd be missing out 
HOST:Emma. 
SPEAKER3: I have no killer balls
And I also have the front two balls the most money on the table
HOST: We've heard all the arguments.
There's only one killer ball on show. We have to try and find out where those other three are
Hugh. Emma. Helen. Owen
You have to decide.
now.
HOST: [CUT] Well, the lying has started early three killers have apparently vanished into thin air
Where can they be? Have they found them or are they making a costly mistake? Hey, we'll find out, after the break. [BREAK]
Welcome back to golden balls our four players have made their decision.
We're about to lose one player and their golden balls will be binned and out of the game.
Helen's got the only killer showing but, where are the other three? I hope for their sake they found them.
It's time to reveal who's been chosen to leave
The first vote is,
For Helen
The second vote is,
For Owen
The third vote is,
For Owen
It could possibly be a tie or Owen is out. the final vote is for
Owen.
Well, it could have gone any which way
Owen for some reason they didn't believe you you were claiming on your back row no killers. That's correct
But you were claiming five hundred pounds and a thousand pounds. Can we see what you have with your five hundred pound ball?
Was he telling the truth?
SPEAKER2: 500 pounds 
HOST: you were. This is a thousand pounds you're gonna have four killers going through with you
Is it a killer or is it what you said one thousand pounds 
SPEAKER2: enjoy those killers.
HOST: Sadly they didn't believe you Owen. Helen. 
SPEAKER1: Yes
HOST: You were claiming no killers at the back row, but you were claiming a
forty-five thousand pound ball and 
SPEAKER1: twelve hundred 
HOST: twelve hundred, let's see the twelve hundred. were you telling the truth?
SPEAKER1: eighteen actually
HOST: Interesting, but it's not a killer now what they want to know is this forty-five thousand pounds or a killer
SPEAKER1: I had to fight my corner big time. Sorry guys
HOST: But that's the way you play golden balls that is very very well played Helen. Hugh you claimed no killer balls
What did you claim? 
SPEAKER4: I claimed eleven hundred pounds six hundred pounds 
HOST: Hugh? Can you reveal the eleven hundred pound ball?
SPEAKER4: eleven hundred pounds. Killer
HOST:And what's this one? 
SPEAKER4: Claimed six 
HOST: six hundred?
SPEAKER4: but it's also a killer
HOST: Woo Emma. You were claiming on your back 
SPEAKER3: eighty pound and five hundred. 
HOST: I don't think you were lying. Let's see the eighty.
And is it five hundred? it is indeed.
Owen, they didn't believe you. you have one further task before you go and that's to bin this
two thousand six hundred pounds. that's five hundred, a thousand and six fifty.
Owen you've binned your balls, but you're out the game. Thanks for playing. 
SPEAKER2: Cheers. 

[ROUND 2]

HOST: Owen has gone back to Pembroke but three players remain
Amanda now loads the twelve surviving golden balls back into the ball machine
And the Golden Bank provides us with two more cash balls.
But of course to spoil the party there's one more deadly killer ball to be added to the mix.
There are now fifteen golden balls in the ball machine killers and cash
Each player now receives five at random.
Three for the back row and two for the front.
Hugh, Emma, Helen 
SPEAKER4: yes
HOST: Congratulations. 
SPEAKER1: Thank you. 
HOST: There were some right shenanigans in that round wasn't there Emma?
SPEAKER3: There was indeed 
HOST: wasn't there indeed? I can tell you that the five top balls you've taken through with you add up to
thirty thousand eight hundred pounds
Including the twenty thousand pound ball and the seven thousand five hundred pound ball the bad news is that you've bought through
Four killers we've added one. That's five killers
You've got to find these killers in this round because you know what they do to your jackpot in the final stages.
Helen 
SPEAKER1: yes
HOST: you were superb in that last round. 
SPEAKER1: I I was starting on a bad note
I had no choice. I know but get myself through. 
HOST: You have got a Popeye tattoo on your backside, don't you?
Can we see what you have on your front row this round, 
SPEAKER1: okay
HOST: there's five killers out there remember that 
SPEAKER1: again my first killer to show 
HOST: Didn't do you any harm last time 
SPEAKER1: well, we know there's five it's bound to happen
But there comes some money
HOST: One of the five killers on show Hugh. 
SPEAKER4:Yes
HOST: you're a barman. 
SPEAKER4: I am 
HOST: ah you served up a right
Cocktail of lies and deceit to these two girls 
SPEAKER4 : not so much. So no
I did paint a picture slightly redder than what it was
I still took about eight and a half thousand through
HOST: touch of Irish charm in the cocktail 
SPEAKER4: perhaps a little bit of learning
HOST: All right, what have you got on your front row this time
SPEAKER4: this time? We're starting with a thousand pounds
And we'll follow that up with a nineteen thousand
HOST: Emma I have to say
Towards the end of that last round your face was going down and down and down like one of your licks
What have you got on your front row, please Emma
SPEAKER3: I've got my first killer
HOST: And I've got five hundred pounds
The twenty thousand pound ball is not on show the seven thousand five hundred pound ball is not on show
Two killers three to find you know how the game is played. We've seen your front row
We need to find out what you've got on the back row
That for your eyes only Hugh Emma Helen look at your back row balls
now 

SPEAKER1: [STATEMENT OFF CAMERA] I Want to get rid of the person that I trust the least I want to make sure I'm with someone that will share in this final
SPEAKER3: [STATEMENT OFF CAMERA] I'm going to pay attention to everything that's going on around me just so that I can get through to the final
speaker4:[STATEMENT OFF CAMERA] If I get to the final I'm going to convince the person to split but I'm stealing the money
HOST: Shortly you're going to have to vote off a second player whose balls will be binned and out of the game for good
The key here is that there's two killers on show, but three killers
Still hiding you need to try and find out where they are
Gonna ask a question
From the last round we had a twenty thousand pound ball through
Anybody claim that?
SPEAKER1: I've got it. 
SPEAKER3: I claim it. 
SPEAKER1: Well, Emma, you know straight away. You're lying. I've got it. 
SPEAKER3: [OVERLAP] I'm not lying
SPEAKER1: Hugh, seriously
SPEAKER3: [OVERLAP] I think you're lying
SPEAKER1: Emma, seriously, I've got the twenty thousand.
SPEAKER3: Seriously, I've got seven thousand five hundred and I've got twenty thousand. 
SPEAKER1: I haven't got the seven thousand five hundred 
SPEAKER3: [OVERLAP] I have [HARD TO DISINGUISH DUE TO OVERLAP] 
SPEAKER1: But, Emma You haven't 
SPEAKER3: I have 
SPEAKER1: you haven't have you 
SPEAKER3: [OVERLAP] I have 
SPEAKER1: because it's sitting in front of me 
HOST: Helen, what you have supporting your 20,000 ball
SPEAKER1: okay, I have got
two hundred and fifty  and I have got hundred
HOST: Emma you claim the twenty thousand pound ball. 
SPEAKER3: Yeah
HOST: what have you got supporting it?
SPEAKER3: I've also got a seven thousand five hundred and I will admit put my hands up. I've got another killer
HOST: So if you're telling the truth, we know where three of the killers are 
SPEAKER3: yep
HOST: but we're still trying to find another two and
Hugh you're not claiming any killers. 
SPEAKER4: I didn't say that. 
HOST: No? 
SPEAKER4: I'll clear one
So I'll twin the girls, but they've either got a pair each or somebody's got three
HOST: What have you got Supporting your killer? 
SPEAKER4: I have eighty pounds, which is not worth a Italy
two thousand five hundred 
SPEAKER1: Emma's
twenty thousand that she's declaring is a killer because I have
SPEAKER3: [OVERLAP] it is not the killer 
SPEAKER1: because I have twenty thousand
SPEAKER3: [OVERLAP] I think you're lying because I have a twenty thousand here
SPEAKER1: You haven't
SPEAKER3: I have. I don't believe you. I think you're completely lying. 
She completely lied in the last round
I asked her if she had a killer in the back if she did then we'll go through she lied then she's lying now
I've got the twenty thousand here. 
SPEAKER1: Seriously. I have the twenty thousand pound ball
SPEAKER4: you've only got one killer. 
SPEAKER1: I've got one killer ball out on the front row. 
SPEAKER3: She lied blatantly in the last round
She's lying now. 
SPEAKER1: I am not lying and you know I'm not lying Hugh at the end of the day
SPEAKER3: I promise you she's lying. Who do you wanna take through someone trustworhty who hasn't lied through this or someone who blatantly keeps lying
SPEAKER1: [OVERLAP] But Emma, You know, I've got the twenty thousand so
SPEAKER3: [OVERLAP]No. Because I've got it
SPEAKER1: [OVERLAP] But you haven't. You haven't got it
SPEAKER3: And I will show it when we reveal the balls in
SPEAKER1: [OVERLAP] Emma, you haven't got it
SPEAKER3: I Have
HOST: Right. Emma Why should they keep you in the game? 
SPEAKER3: I've got two killer balls
But I also have the twenty thousand seventy five hundred Helen lied in the last round
You've got to go with me on this time because she's lying this time 
HOST: Helen
Why should they keep you in the game? 
SPEAKER1: I did lie in the last round
But this time I hold the biggest amount of money that we know is gone through I
Have that twenty thousand and I don't have another killer ball 
HOST: Hugh, 
Why should they keep you in the game? 
SPEAKER4: I think that's done and dusted. I'm staying. I just have to pick which lady I'm taking with me
HOST: Three of you
have to decide because someone is leaving golden balls and
Binning their golden balls out of the game you need to get rid of the killers
But you've got to keep in the cash Hugh Emma Helen you have to decide now
The finger of fate is indeed fickle and it's pointing at one of our players
Which one?
Hugh seems to have put himself in the driving seat, but is he being too complacent?
Helen and Emma are both swearing blind. They have the twenty thousand pound ball
One of them's lying which one? join me after the break to find out who's leaving golden balls [BREAK]
Welcome back to golden balls for the second time our three remaining players have made their decision
One player now has to leave the game and their golden balls will be binned
Last time they found no killers for the sake of their jackpot. Let's hope they had this time
It's time to reveal who's been chosen to leave,
There is one vote,
Emma.
There is one vote,
Well Helen.
Ladies it's unlikely to be a tie
The final vote is. Emma.
Emma Sorry about that. 
SPEAKER3: That's okay. 
HOST: You were claiming one more killer ball. Yeah, could we see that?
Seven thousand five hundred is that there? 
SPEAKER3: That wasn't a lie either
HOST: now There was contra jumps between you and Helen about the twenty thousand pound ball. Who's lying?
SPEAKER3: Me
HOST: Still a killer to be found 
SPEAKER3: should have believed me
HOST: Helen 
SPEAKER1: yes
HOST: you were claiming no further killers
We know I would imagine it's safe to say that you have and Hugh's 
SPEAKER1: Nodding in agreement 
HOST: nodding in agreement that you have the twenty thousand pound ball. 
SPEAKER1: I have 
HOST: let's have a look at that
SPEAKER1: It was in the middle all the time
HOST: You claimed two hundred and fifty. let's see it. 
SPEAKER1: I Was being a little bit more honest this round
HOST: You know the answer here Hugh. 
SPEAKER4: Yeah, 
SPEAKER1: yeah, 
HOST: is it a killer?
SPEAKER1: It is a killer, but we've got the big money in there 
HOST: alright Hugh You admitted a killer 
SPEAKER4: I did 
HOST: is that there 
SPEAKER4: it is and I give the ladies every opportunity to admit theirs
HOST: 80 pounds. I would imagine that's there. 
SPEAKER4: Yes There we are
HOST: two thousand five hundred is that there? 
SPEAKER4: Well, it's not actually
HOST: Hugh, you said that you believed Helen, but you trusted Emma
Why the decision went for the money? 
SPEAKER4: Yeah, I just had a feeling
The eighteen hundred  I didn't believe
Was money? I thought it was the killer and I thought she had three killers there
And I was looking to get rid of the killers rather than keep money 
HOST: Emma Sadly, you're gonna have to leave us one last thing to do before you go
That's to bin your golden balls start with the killers because they'll be pleased to see those go
And get rid of the seven thousand five hundred
SPEAKER3: bye bye
HOST: good money, one thousand eight hundred
And the five hundred. you've binned your balls, but you're out the game. Thanks for playing
SPEAKER3: Thank you. 

[ROUND 3]

HOST: [CUT]ten Golden balls remain in play
Seven cash balls and three killer balls
They're going back into the ball machine and one final golden ball is being added
As always it's a killer ball. But Somewhere in those eleven balls we know are the five cash amounts that can make up your maximum potential jackpot tonight
We've seen them already. They are
two hundred and fifty, six hundred, one thousand, nineteen thousand and that twenty thousand pound golden ball
Helen and Hugh you've seen off the competition
You now have to work together
To accumulate as much cash as you can for your jackpot today. It's time to play bin or win
You. You. congratulations. your maximum potential jackpot for tonight is forty thousand eight hundred and fifty pounds.
twenty thousand pound ball in there a nineteen thousand pound ball, but also
Four killers and you know what they can do. You now have to play bin or win.
You have to pick a ball to bin. whatever it is it goes out of the game whether it's that twenty thousand pound ball or the killer ball
Then you have to pick a ball to win. if it's a cash ball
It goes into your golden five if. it's a killer ball
It goes into your golden five after you've accumulated
Whatever it is and you pick a killer ball into your golden five it
Decimates the total. so for instance if you have
twenty thousand pounds worth of cash that you're playing for and then you pick a killer
That twenty thousand pound goes down to two thousand pounds
Another killer and it's down to two hundred another killer and it's down to twenty
That's why you've been avoiding those killers, but you have bought through three and we've added one
Helen. 
SPEAKER1: yes. 
HOST: you have bought through the most amount of money tonight, so you start. you have to pick a ball To bin it would be great if you could bin a killer to start with 
SPEAKER1: it would be 
SPEAKER4: okay
SPEAKER1: Okay Hugh, shall I just go with the nearest 
SPEAKER4: whatever takes your fancy you lift one that's winking at you or that's giving a little twinkle or whatever. 
SPEAKER1: I'm going to bin this one
HOST: Why?
SPEAKER1: because it's the nearest to me and my eyes went straight at it
HOST: Let's see if you're right. Let's hope it's a killer
SPEAKER1: it's a killer
SPEAKER3: That's what we want. That's what we want
HOST: Get it out the way whoo it's gone. Okay, only three to five
You need to pick a ball to win. 
SPEAKER1: Okay, it's time. I think I'm gonna go in the middle
SPEAKER4: Let's hope. 
SPEAKER1: Fingers crossED Hugh 
HOST: why the middle?
SPEAKER1: Play it safe
HOST:You happy?
SPEAKER1: Yeah
SPEAKER4: Have to be
HOST:too late now. 
SPEAKER1: Yeah. Fingers crossed Hugh
HOST: two hundred
SPEAKER4: [OVERLAP]That's good
SPEAKER1: [UNINTELLIGIBLE] We're doing it the right way. Okay, come on. 
HOST:Go to your top five your total cash to win so far is 
SPEAKER1: two hundred
HOST: [OVERLAP] two hundred pounds. Hugh 
SPEAKER4: yes 
HOST: your turn pick a ball to bin
Three killers left good chance of picking one
SPEAKER4:You know for some reason that little lines calling me so I'm gonna go 
SPEAKER1: okay 
SPEAKER4: with this one. I'm 
SPEAKER1:work for me. 
SPEAKER4:It's my night. So 
SPEAKER1:come on 
SPEAKER4:one way or the other. It's going
So it is and it's a thousand pounds
SPEAKER1:Okay, 
HOST: it was in your top five. It's a third highest ball, but it has to be bin. 
SPEAKER4:[OVERLAP]it was, it was. Yep. 
SPEAKER1:Okay
SPEAKER4:in it goes
HOST: Pick a ball to win
SPEAKER1: come on
SPEAKER4:We do want to find those five digits
SPEAKER1:Exactly 
SPEAKER4:I don't want to find those five digits so early. 
SPEAKER1:Okay. 
SPEAKER4:We want to get rid of the killer. So I'd rather have a killer now
And for some reason this one yeah? 
SPEAKER1:yeah
SPEAKER4:sure? 
SPEAKER1:Okay. 
SPEAKER4:Yep. 
SPEAKER1:Well, yeah, we're happy with that
HOST: Only 19,000 pounds
Second highest ball on the table your total cash now is nineteen thousand two hundred pounds. 
SPEAKER1:Yeah
HOST: Your highest possible win is forty thousand and fifty pounds a lot of money
Helen pick a ball to bin. 
SPEAKER1:Okay, so I'm gonna take this one 
SPEAKER4:sure That one 
SPEAKER1: are you happy with that? 
SPEAKER4:I'm 
SPEAKER1:[OVERLAP]I don't know why I went there. 
SPEAKER4:That's fine. If it's down what you feel what one's[HARD TO DISTINGUISH DUE TO OVERLAP] 
HOST: Your intuition was good first time. 
SPEAKER4: Yep. First time was perfect
HOST: Come on, it's got to be a killer. Come on
SPEAKER1: ah, I've got a killer
HOST: That's a high five. 
SPEAKER1: Thank you. Luck of the Irish Hugh, come on 
HOST: good only two killers left out there
SPEAKER1: Okay, okay 
HOST: your total cash at the moment is nineteen thousand two hundred. a killer obviously will take it down to
one thousand nine hundred and twenty
SPEAKER1: we don't want to do that. 
HOST: We don't want to do that. 
SPEAKER1: We don't want to do that. We want cash cash cash cash
HOST: Helen pick a ball to win
SPEAKER1: And it's here
HOST:okay
SPEAKER1: okay
HOST: Whoo, I was hoping we don't want a killer. Okay, there's a twenty thousand pound ball out there
It ain't a killer
SPEAKER1: still cash
SPEAKER4: [OVERLAP] that's good, cash is what we're looking for
HOST: Was the lowest cash ball on the table 
SPEAKER1: but cash
HOST: Your total cash now is nineteen thousand two hundred and eighty. we still have two killers out there
Hugh you really do need to try and find that killer ball if you can. pick a ball to bin
SPEAKER4: This one 
HOST: [OVERLAP] Pick a ball to bin
SPEAKER4: is going
HOST:come on killer killer killer. 
SPEAKER1: Come on Hugh
SPEAKER4: Not what we wanted Helen
SPEAKER1: Oh Well, oh well c'est la vie
SPEAKER4: not what we wanted at all
SPEAKER1: But, don't find the killer. Let's go with the cash 
HOST: [OVERLAP] You're highest possible in now is twenty thousand one hundred and thirty
SPEAKER1: okay 
HOST: still a lot of money
If you can avoid picking the killer ball pick a ball to win
SPEAKER1: I know where my eyes are drawing to and That one 
SPEAKER4: [OVERLAP] that one
SPEAKER1: you were with me on that one. same movement. 
SPEAKER4: Yep. 
SPEAKER1: All right. We'll both take that one through 
SPEAKER4: Right, Jasper. That's cash
HOST: Helen Hugh if this is a killer ball you're nineteen thousand two hundred and eighty goes down to one thousand nine hundred and twenty-eight 
pounds, It means you'll be playing for tens of thousands or hundreds
We're gonna find out after the break [BREAK]
Welcome back to golden balls. in my hand I have the golden ball that Hugh gave me
To put into his golden five if it's a killer ball
The total cash they have amounted so far of
nineteen thousand two hundred and eighty pounds goes down to
one thousand nine hundred and twenty-eight pounds
Are you ready? 
SPEAKER1: Yeah
HOST: It's money, it's not a killer. 
SPEAKER1: No 
HOST: your total cash now is nineteen thousand five hundred and thirty pounds your highest possible win is twenty thousand one hundred and thirty
Two killers, 
SPEAKER1: okay
HOST: two to one chance. Helen pick a ball to bin
Needs to be a killer. 
SPEAKER1: Okay. Hugh did mention you had a feel that that was not good near you
SPEAKER4: I don't know why I don't like this ball. 
SPEAKER1: You've got a bad vibe about it. 
SPEAKER4: I don't know why 
SPEAKER1: all right. I'm gonna take your vibe my eyes are drawn to it as well
I'm gonna bin, This one
HOST: You know if this is the six hundred pound ball
You're gonna be having a cash total to fight over of one thousand nine hundred and fifty three pounds. 
SPEAKER1: Okay. 
HOST: Let's hope it's a killer come on
SPEAKER1: Right, are we ready?
SPEAKER4: That's what we wanted
HOST: This is an eighteen thousand pound decision. you need to pick a ball to win
SPEAKER1: That vibe was right. 
SPEAKER4: Yep
SPEAKER1: okay, My vibe for money is in front of you. 
SPEAKER4: Yes. 
SPEAKER1: Okay decision made. Before we discuss it too much
HOST: It is in fact a seventeen and a half thousand pounds decision
I'm rooting for you. I don't want this to be a killer. 
SPEAKER4: [UNINTELLIGIBLE]it has to be
HOST: Are you ready? 
SPEAKER1: Yeah
HOST: Yeah, it's the six hundred. What's this year is the killer boss, yes
You Helen your jackpot tonight is twenty thousand one hundred and thirty pounds
Incredibly Helen and Hugh have managed to dodge four killers and now they have
twenty thousand one hundred and thirty pounds in front of them. We know they've got it the golden question is
Can they keep it?
You know there's a very straightforward choice
But it's a choice that could make one or both of you some serious money
But it could also lose you everything that you fought for today
Helen, Hugh, you have to decide to split or steal
The two of you now
Have one more golden ball each to open. They're not cash. They're not killers. You each have a golden ball with the word
split written inside.
And you each have a golden ball with the word steal
written inside.
You will know which one you are choosing
If you both choose the split ball
You will take home each ten thousand and sixty five pounds.
If one of you chooses steel and one of you chooses split whoever chooses the steel ball will go home with
twenty thousand one hundred and thirty pounds pounds and the other one will get nothing
But if you both choose to steal
You both go home with nothing a big fat zero
Are you aware of what I'm saying? 
Yes, there's some serious money here
SPEAKER4: [OVERLAP] Absolutely. Yeah, absolutely
SPEAKER1: [OVERLAP] yeah, fully aware
HOST: I want you first and foremost to check each ball to make sure you know
Which is split and which is steel. Please have a look now
Are you both satisfied you know which is split and still 
SPEAKER1:yeah
SPEAKER4: jup 
HOST: okay? I'm going to give you a little time together to discuss
What you would like to do?
Helen you kick it off. 

[SPLIT/STEAL]

SPEAKER1:Okay. This has been the most amazing journey. We've lied. We've cheated
We've blacked our way through. Yep. It stops now
ten thousand pounds to me is colossal. 

SPEAKER4:We either go home with nothing each or we split the money
SPEAKER1:There is no other choice for us to both walk away from this place happy
We've got a split. 
SPEAKER4:We have to split and we have to take ten grand each. Let's split the money between us. 
SPEAKER1:Yeah, believe me I will 
SPEAKER1:[OVERLAP]really split the money between us
HOST: It's the easiest choice, but it's the most difficult. 
SPEAKER1:It is. It's a strange choice
Because we both know what we want the other to do 
HOST: no more talking 
SPEAKER1:no more
HOST: Looking to each other's eyes. 
SPEAKER1:Okay
HOST: Helen, Hugh choose split or steal now. Split or steal
Helen, Hugh you've just one ten thousand and sixty five pounds each, well done.

[END]

SPEAKER4:Absolutely fantastic.
HOST: And I want to see Popeye on your backside. What a fantastic show.
SPEAKER4: [UNINTELLIGIBLE]
HOST: I'm now declaring you friends for life. [CUT] They've been lovely. I've been Jasper Garrett. This has been golden balls. What a fabulous show
Until next time. Goodbye
SPEAKER1:I am absolutely delighted. I cannot tell you how happy and what a difference this is going to make to me
SPEAKER4:We did lie and we both had killer balls in the first round. We had to get rid of them
You just know sometimes when somebody's gonna play along and I'm glad Helen did
