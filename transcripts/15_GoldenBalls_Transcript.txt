HOST: [CUT]Hey, what do we want to see more of on television? We want to see honesty.
Speaker1: My hand's on my card. That's fifty thousand pounds. She's telling lies. She is lying.
HOST: [CUT] We want to see truthfulness.
SPEAKER2:If you think I'm lying, you really, you need to work on your instinct, look. I am telling
the truth one hundred and ten percent.
HOST: We want to see good old-fashioned manners.
SPEAKER3: [CUT] You want to go through and you want to trust some person to go through, because at the
end of the day, I'm going to split.
HOST:[CUT] One day, it'll happen. Meanwhile, here's Golden Balls.

[ROUND 1]

HOST: [CUT] Hi, welcome to the show, where you can win a four-shoot, but only if you've got the
balls. Yes, it's Golden Balls. Once again, we have four players looking to bluff their
way to the big money, a human resources manager, a railway worker, a team organiser, and an
artist. But who's got what it takes to take the lot? And we know what it's all about.
That's right, the Golden Balls. And we have a hundred more just like this one behind me
in the Golden Bank.
Let's not keep them up there too long. Start the ball machine. Ah, the Golden Bank. Stuff
full of riches. Golden Balls, all filled with money. We know one of them's worth ten pounds.
And we also know that one of them is worth seventy five thousand pounds.
Well, how much is coming out to play today? Who knows? But one thing we know is that bad
blonde ball killer Amanda is there to put four of her killer balls in amongst them.
And so, there they are, the first sixteen Golden Balls of today's game. Cash and killers. And
as always, each player receives four balls at random.
That's two for the front row, and two for the back.
Welcome everybody to Golden Balls.
SPEAKER1: Oh, Jasper.
SPEAKER4:Jasper.
HOST: Great to see you. Four people here, mouths full of frozen butter. sixteen Golden Balls, four
of them killers, twelve of them cashed. Where are they? Let's find out. Our first player today is Beverly from Aberdeen.
HOST: Hi Beverly
SPEAKER4:Hi
HOST: Lovely to have you on the show.
SPEAKER4:Thank you.
HOST: What do you do?
SPEAKER4:I'm an HR manager for an accountancy firm. I work in training and development, so it's
student development, working with new graduates that come into the firm.
HOST: Train young accountants?
SPEAKER4:Yes.
HOST: What do you train them?
SPEAKER1:In sort of technical skills and soft skills.
HOST: Soft skills? Is that Play-Doh?
SPEAKER4:That's kind of touchy-feely skills, Jasper.
HOST: Right, I'm a bit thick.
Now, of course, you wanted originally to be a vet.
SPEAKER4:I did.
HOST: But you don't like seeing sick animals.
SPEAKER4:That's true.
HOST: A bit of a quandary if you're going to be a vet, isn't it?
SPEAKER4:I know, I know.
HOST: I mean, you don't have vets for well animals, do you?
Hello, hello.
My dog's really well.
Bring him over.
Would you like to see what's in your front Golden Balls?
SPEAKER4:I've got an eight hundred pound ball.
HOST: Okay.
SPEAKER4:And I've got a killer.
HOST: Okay, one killer on show.
But there's four of them out there, Beverly.
Nothing to worry about.
They could be anywhere.
I wonder if our next player's got any killers on his front row.
It's Johnny from Akrington. Hi John
Lovely to have you here.
What do you do?
SPEAKER1:I'm an artist.
HOST: And you earn a fair bit of money.
You sell your paintings?
SPEAKER1:I do, yes.
On eBay or...?
SPEAKER1: No, no, no.
HOST: [OVERLAP]No? No? Local exhibitions, private buyers.
SPEAKER1:My paintings are very surreal, actually.
I do get a lot of sort of inspiration from sort of horror,
gruesome, blood.
Very three-dimensional.
HOST: You've got a painting 
SPEAKER1: yeah
HOST: of Jack the Ripper
SPEAKER1:yeah
HOST:attacking your ex-wife. 
SPEAKER1: Jup
HOST: What inspired that?
SPEAKER1:Well, actually, it looks like my ex-wife.
I don't think we should talk about the ex-wife.
She might be one.
HOST: Second person up today.
What have you got on your front row there, John?
SPEAKER1:OK, Jasper.
We have got an amazing one thousand seven hundred fifty.
HOST: Very nice.
And the next row...
SPEAKER1:Wow. Four thousand.
HOST: Our third player is Kelly from York. Hi, Kelly.
SPEAKER2:Hi.
HOST: What do you actually do, Kelly?
SPEAKER2:I'm a team organiser for Network Rail.
HOST: Oh, an organiser for Network Rail?
SPEAKER2:Yes.
HOST: Network Rail Organised. Hmm.
SPEAKER2:I know.
HOST: So what do you organise?
SPEAKER2:I basically look after a team of about sixty five people,
and I just organise events and meetings.
HOST: You have a very strange phobia.
SPEAKER2:I'm scared of chalk.
HOST: How? Why? Where? Chalk?
SPEAKER2:I don't know. It's chalk and charcoal.
HOST: I bet you love cheese.
SPEAKER2:I love cheese.
HOST: Yeah, how did I know that?
And also, you have this phobia about people standing
on your left hand side.
SPEAKER2:Yeah, I'm not keen on people
being stood next to me on my left.
HOST: OK.Let's see what you have on your front row, please, Kelly.
SPEAKER2:OK. The first ball I have is four hundred pounds.
All right, it's not a killer.
Not a killer.
HOST: And the second ball is? 
SPEAKER2: one thousand nine hundred pounds.
HOST: Our four players have been watching very keenly
what's going on here.
It's Dean from Swindon.
Can you give yourself a clap, please?
SPEAKER3: Thank you. Pleased to meet you.
HOST: [OVERLAP]Lovely to meet you, my friend.
What do you do?
SPEAKER3:I work on the railway for Network Rail in Swindon.
HOST: Oh!Do you two know each other?
SPEAKER3:No, no, no.
HOST:You haven't got any chalk on you?
But you are sitting on a left hand side.Oh!So you work on Network Rail.What do you do?
SPEAKER3:I've got lookout duties and I also do maintenance.
HOST: All right, so you basically...
Everybody works and you tell them there's a train coming.
SPEAKER3:Yeah, yeah, yeah.
HOST: One of your pet hates is bad liars.
SPEAKER3:Oh, yes, definitely.
HOST: You can tell them a mile away.
SPEAKER3:I can tell them within a mile.
Yes, I'll be able to suss them out today.
Don't worry about that.
HOST: You're all warned, OK?
Dean, last person up.
What have you got on your front row?
SPEAKER3:What I've got today is...
HOST: Oh!
HOST: ten thousand pounds!
SPEAKER3:Come on!Yes! Happy with that, happy with that.
HOST: I should think you are.
And your second ball is...
SPEAKER3:Unfortunately a killer, but not too bad.
Not too bad.
HOST: Two killers on show, two defined.
This is only half the story.
You each have two balls on the front row, which we can see.
But you have two balls on the back row, which we can't.
They're for your eyes only.
When I ask you to have a look,
have a look but keep them to yourselves.
Johnny, Beverly, Dean, Kelly.
Have a look at your back row balls now.
SPEAKER4: [STATEMENT OFF CAMERA] Today on Golden Balls I'm going to play the game like I danced the tango with passion and with flair
and ideally I'll find my perfect partner for the fight.
SPEAKER1:[STATEMENT OFF CAMERA]On Golden Balls today I'm going to be spontaneous and unpredictable.
Any contestant with no integrity or ginger hair I will be voting off.
SPEAKER2:[STATEMENT OFF CAMERA]I did my university dissertation on reading body language and deceit
so I'm hoping that I'll give me an advantage in reading my fellow players today.
SPEAKER3:[STATEMENT OFF CAMERA]On my railway lookouts my instincts are very good.
If any of the players are out of line today I'll be there to suck them out.
HOST: Eight Golden Balls still to be revealed.
We know that two of those eight are killers but there's also six cash balls to be revealed.
Kelly, what have you got?
SPEAKER2:I've got fifty thousand pounds in eleven.
HOST: You what?
SPEAKER2:I've got fifty thousand pounds in here and I've got eleven thousand  pounds in here
and I'm absolutely bursting with excitement.
HOST: Well obviously.Jhonny, you need some chalk quick. What have you got?
SPEAKER1:Honestly, I have a killer and I also have three thousand pounds.
HOST: Alright. Beverly, what have you got on the back row?
SPEAKER4:I don't have any more killers.
HOST: No.
SPEAKER4:I've got eight hundred and fifty in this ball and I've got a thousand and five hundred in this ball.
HOST: Right, Dean.
SPEAKER3:My back row I've got six thousand pounds and I've got another killer.
HOST: Okay, so if everyone's telling the truth, we know where all the killers are.
Now it's a case of who do you believe has got what they say they've got on the back rows.
Beverly, do you believe Kelly?
SPEAKER4:No.
HOST: No. 
SPEAKER4:No. 
HOST: What don't you believe?
SPEAKER4:I think if I had that much money on my back row I'd be a lot more excited than Kelly is right now.
SPEAKER2:I was leaping out of my seat when I opened them. I cannot control my excitement more.
I honestly, honestly have 50,000 pounds here and 11 here.
SPEAKER3:I totally agree with Beverly.
SPEAKER2:I am so excited about it.
SPEAKER4:I do believe that you're telling one porky boy somewhere.
SPEAKER2:I'm honestly not telling any pockets. This is fifty thousand pounds. Guys, don't be silly and throw it away.This is eleven.
SPEAKER1:Actually, I do actually believe Kelly.
SPEAKER2:Thank you.
HOST: Right.
SPEAKER1:Just a body reaction, a face posture.
I think she's telling the truth.
I really do.
SPEAKER4:I could say Kelly, I think, I don't know, I just think I'd be a lot more excited about that kind of money.
SPEAKER2: [OVERLAP]I cannot possibly be any more excited.
I was about to leap off my seat when I opened these.
I nearly wet myself with excitement.
SPEAKER4:At the moment, I'm not a hundred percent with you.
SPEAKER2:[OVERLAP]I've got no reason to lie.
SPEAKER3:What have you got, Johnny in the background?
SPEAKER4:Johnny's very quiet, which makes me wonder.
SPEAKER1:[UNINTELLIGIBLE] And a killer.
SPEAKER3:three thousand and a killer.
SPEAKER1:Yeah.
SPEAKER3:So we've got...Yeah, okay.
SPEAKER1:There's no point to lie because we found the four killers.
And I think it's between Dean and the lady on my right.
SPEAKER2:You want to take through cash on [UNINTELLIGIBLE]
SPEAKER4:if, if that ball at the back is what you say it is.
SPEAKER1:three thousand?
SPEAKER4:Yeah.
SPEAKER1:Yeah, of course it is.
SPEAKER2:I don't think you've got any reason to lie.
SPEAKER2:No.
SPEAKER4:No, I don't know.
My instincts are telling me that there's a fabricated amount somewhere over there.
One of those balls.
Definitely.
SPEAKER1:Well, I can't be fabricating a killer.
I think you're kind of deflected, aren't you, Dean?
SPEAKER3:No, not at all.
Not at all.
SPEAKER2:[OVERLAP]I don't think Dean's lying.I think he's upset.
SPEAKER3: [OVERLAP]I'll be totally honest all the way through because I want to go through.
And you want to trust some person to go through.
Because at the end of the day, I'm going to split.
So there's no reason not to trust me.
And that's exactly how I feel.
And I've told everybody that.
SPEAKER4:Okay, with you, there's no...
I could have overinflated the event.
SPEAKER3:Oh, give you as well, Beverly.
SPEAKER4:I could have overinflated the amount.
SPEAKER3:[OVERLAP]Yes, yes, yes.
SPEAKER4:But I'm not going to do that.
Because at the end of the day, this is paper money until you get through the final.
SPEAKER3:[OVERLAP]I believe it's either Johnny or Kelly as I think.
SPEAKER2:Do you want to take through killers or do you want to take through cash?
Because in this case, we've got two killers and sixteen thousand.
In this case, we've got one and about three thousand.
SPEAKER1:It depends on the honesty and sincerity.
SPEAKER2:[OVERLAP] I do honestly think everyone here has been truthful.
I don't think anyone's lying.
I don't think anyone's got an awful lot.
I mean, Dean certainly wasn't.
SPEAKER3: [OVERLAP] Or do you have to come up with a killer?
Or do you have to come up with a killer?
SPEAKER1:Two killers, though. Two killers.
SPEAKER4:At the end of the day, Dean, everybody It's about who you trust, isn't it?
[HARD TO DISINGUISH DUE TO OVERLAP] 
SPEAKER3:Exactly.
SPEAKER4:It's about that you take killers into the next round.
SPEAKER2:[OVERLAP]I will honestly, honestly say to you guys now, this is fifty.
This is eleven. Hand on my heart now. You're throwing away sixty-three thousand pounds if I go. That is a lot and a lot of money.
SPEAKER3:Oh, totally.
SPEAKER2:If you think I'm lying, you really, you need to work on your instincts, mate.
I'm telling the truth one hundred and ten percent.
SPEAKER3:That's fair enough. Logs are all along. I do trust my instincts.
SPEAKER2:I won't put them on this case, mate.
HOST: So, Dean, do you not believe that Kelly has that amount?
SPEAKER3:I don't believe two amounts.I mean, fifty thousand.
SPEAKER2:I've been very, very, very fortunate.
And I just happen to have been given the two because both.
SPEAKER3:I just don't think it's right.
SPEAKER2:I've got no reason to lie to that.
I've got no killers.
We've established that.
And I've got a pretty decent front row.
So I don't really have...
SPEAKER3:You've got to get this right because you want to get through, basically, don't you?
SPEAKER2:But I'm not going to lie.
SPEAKER3:No, no, but no, I'll do.
SPEAKER2:If this wasn't fifty grand, I wouldn't be this safe.
SPEAKER3:What's the other one hundred fifty grand?
SPEAKER2:No, this is fifty.
This is eleven.
I've said that all along.
fifty, eleven.
fifty, eleven.
SPEAKER3:No, I'm sorry, but...
HOST: Beverly, why should they keep you in the game?
SPEAKER4:Because if they want somebody that they can trust in the final,
I'm playing an honest game.
HOST: Johnny
SPEAKER1:great amount. Honest, all the way through. Split all the way.
HOST: Dean, 
SPEAKER3:I've been hundred percent honest.
And I believe I've got a nice ten thousand pound ball there
that should be enough to get me through.
HOST: Kelly?
SPEAKER2:I've got fifty thousand pounds here,
and I've got eleven thousand pounds here.
Hand on my heart, guys.
These are honestly, honestly here.
HOST: You have one vote each.
Someone has to leave golden balls.
You have to decide who it is now.
Thank you, Amanda.
[CUT]The players have decided which one of them is leaving the game.
Well, I don't know what to say.
They all seem so honest.
I almost claimed a killer ball myself.
Do they know how to play the game?
We'll find out after the break.[BREAK]
[CUT] Welcome back to Golden Balls. The votes are in. Our four players have decided who is about
to leave the game and see their Golden Balls win. Before the break, the players seem to
have found all the four killers. Now, have they found the person they can trust? It's
time to reveal who's been chosen to leave. The first vote is for. Dean. The second vote
is for. Dean. The third vote is for. Kelly. The fourth and final vote is for.
Kelly. It's a tie. Two votes for Kelly, two votes for Dean. In this situation, the players
that have received no votes, that's Beverly and Johnny, will now discuss between themselves
which one of these two will leave the game and take their balls with them. Johnny, Beverly.
SPEAKER1:What's your opinion? You take two killers or you take cash? 
SPEAKER4:I'm sorry, Dean. When it comes down to it, eliminating you takes two killers out of the game. I'm not sure, Kelly,
whether you've got the amounts that you said you had, but whatever, you've got cash. So,
I would go with Dean. 
SPEAKER1:He looks like an honest person, but the two killers is a downfall.
SPEAKER4: yeah
SPEAKER1:Sorry, Dean. 
HOST: What is your decision, Beverly? 
SPEAKER4:Dean to leave. 
HOST: Johnny? 
SPEAKER1:Unfortunately, Dean.
HOST: Dean, you have to leave Golden Balls, sadly. You admitted a killer. Can we see that?
SPEAKER3:Killer ball.
HOST: And six thousand pounds. 
SPEAKER3:And six thousand pounds. 
HOST: There it was. What we really want to know, Kelly, is you were claiming in this ball eleven thousand pounds. Is that there? 
SPEAKER2:Absolutely. Honest to the day's one, guys.
HOST: Now, this is crucial for your integrity and their relief. Show us what that final ball is. 
SPEAKER2:Do you think it is? Absolutely. 
HOST: It's fifty thousand pounds. You look a bit relieved there, Beverly. You were claiming eight hundred and fifty thousand. Is that there? 
SPEAKER4:eight hundred and fifty thousand. 
HOST: And a thousand five hundred ball. Is that there? 
SPEAKER2: Absolutely.
HOST: Johnny, you are claiming a killer on the back, which must be there. 
SPEAKER1:That's very true. This is the killer.
HOST: And three thousand pounds. Is that there? 
SPEAKER1:Unfortunately, I left my glasses at home. 
HOST: Oh, thirty pounds. 
SPEAKER1:It's only a naught?
HOST: All right. Well, it's two naughts. I've got an o -level in maths, you see. Dean, sadly, it's always tough
when you had two killers. You took a chance by admitting the second one, hoping that they'd
take you through on your honesty. 
SPEAKER3:It's always best to be honest, but I lost down. 
HOST: There you are. But you've been a great player. It's been a pleasure having you here. One final task
before you go. Can you bin your golden balls, please? It goes ten thousand pounds killer. six thousand pounds killer.
It has to go. 
SPEAKER3: hate to see them go, but that's part of the game.
HOST: Dean, you've binned your balls and you're out the game. 
SPEAKER3:Thank you, Jasper. 
HOST: Thank you.
[CUT] Honest Dean is out of the game and goes home to Swindon. Well, that is if his train turns
up on time. The twelve remaining balls go back into the ball machine and the golden bank adds
two more mystery cash balls. And of course, Amanda goes and spoils the party with another
one of her killer balls. So there's now more money in the game, but there's also one more
killer. You now have fifteen golden balls in play, killers and cash. Each of our players now
receives five balls at random. That's two for the front row and three for the back.
Well, without Kelly's perceived low-key enthusiasm, you would have got sixty three thousand pounds into the bin. However,
the good news is you have bought through from the first round 72,230 pounds, including the
eleven thousand pounds ball and the fifty thousand pound ball. You didn't bring through four killers. You bought through
two and we've added one, so there's three killers to find. Interesting game. Could be. Very,
very big money towards the end. Kelly, you didn't have any killers last time. 
SPEAKER2:I didn't, no.
HOST: Let's see how you fared this time. 
SPEAKER2:Oh, my friend's come back. eleven thousand cash. 
HOST: eleven thousand. Very nice. The
golden cash. 
SPEAKER2:Let's find out. 
And killer. 
HOST: Johnny, you had a killer on your back row last
time. 
SPEAKER1:Right. 
HOST: Have you got a killer on your front row this time? 
SPEAKER1:Let me have a look for you, Jasper. Not a killer. 
HOST: a thousand seven hundred and fifty. 
SPEAKER1:A killer. 
HOST: One to find.
SPEAKER1: One to find 
HOST: Beverly, if you have a killer on the front row, the three of you have got some very hard work to do.
SPEAKER4:I've got a killer. And I've got 200 pounds. 
HOST: So we know where the killers are. There's still a lot of cash
to find and it's all on the back rows. Have a look at your back balls. They're for your
eyes only and have a look now. 
SPEAKER4:[STATEMENT OFF CAMERA] Ideally, I'd like to be up against a woman and I would
definitely split. But if it's a man, who knows? 
SPEAKER1:[STATEMENT OFF CAMERA]I'm a great living karma. If you live a good
life, good things will happen. I am a winner, not a sinner, and I am definitely going to
split. 
SPEAKER2:[STATEMENT OFF CAMERA]If I'm lucky enough to get through to the final, I'm hoping I'll be against a
male. This is because I come from a male-dominated family and I find that they're much, much
easier to read and they're much easier to tell in a lie. 
HOST: I'm sure you know what TLC
means. Tender, loving, care. In this round, it means trust, lies, cash. It's not about
killers. We know where they are. 
Kelly, what have you got on your back row? 
SPEAKER2:I've got thirty pounds, which Johnny had last time. eight hundred, which Beverly had, and four thousand, which Johnny
had last time. 
HOST: Beverly, what have you got on your back row? 
SPEAKER4:That's what I wanted to see. Actually, I think you might have passed your ball to me. I've got the fifty thousand. I've got
your thousand nine hundred and then in this one, I've got, what was it, eight hundred and fifty, which was mine from the
first round. 
HOST: That enthusiasm enough for you, Johnny? Well, I think she's a bold faced liar. fifty thousand, ten pounds, one thousand five hundred.
SPEAKER4:I've got the fifty thousand
SPEAKER2:that ten pound's a new ball. We didn't have that in the last one. We didn't have two hundred either in the last
Who's got my four hundred pound I had in the last one. 
SPEAKER4:I Haven't got it. 
SPEAKER1:You must have it. 
SPEAKER4:I've got a thousand nine hundred. 
SPEAKER1:You've got four hundred
And i've got the eight fifty. 
SPEAKER2:Someone's got my four hundred that I had in the last round. 
SPEAKER1:What she's trying to say is that the fifty thousand what I've got
She's trying to make out about four hundred
You've got four hundred. 
SPEAKER4:I haven't got a four hundred pound ball Johnny. I haven't I've got I haven't got a four hundred pound ball
But what I do have is the fifty thousand pound ball, which we know is in the game somewhere
SPEAKER1: fifty thousand pounds
SPEAKER4:But what you also know Kelly is that I told the truth last time, but He didn't 
SPEAKER2:you did tell the truth, but you also didn't believe me, but you lied and believed me
So this is my dilemma now. 
SPEAKER4:Yeah, and I can understand
SPEAKER2:[OVERLAP] but yours was a little white lie to be fair, it wasn't a ginormous one, but a lie is a lie
[HARD TO DISINGUISH DUE TO OVERLAP] 
SPEAKER4:Kelly and at the end of the day
SPEAKER2: [OVERLAP]You were truthful
SPEAKER4:is it honesty and I did I did doubt it, because you didn't leave out of your seat
Because I it was just like 
SPEAKER2:[OVERLAP]I did have a little glance up when I saw you open it
SPEAKER4:I am very and I understand what you now say about you're nearly wet yourself. 
SPEAKER1: She's telling lies. I've got fifty thousand pounds, Seriously my heart, my hands on my heart. That is 50,000 pound. She's telling lies 
SPEAKER4:Johnny 
SPEAKER1: She is lying 
SPEAKER4:Johnny I've got I don't have to lie. 
SPEAKER1:[OVERLAP]I cannot believe 
SPEAKER4:I don't have to lie and I didn't lie last time, but you did
SPEAKER1: You look Kelly in the face and say I've got fifty thousand pounds. 
SPEAKER2: i doesn't matter [HARD TO DISTINGUISH DUE TO OVERLAP]to me that doesn't mean an awful lot
SPEAKER4:Kelly, I've got it fifty thousand pound. Johnny i'll look you in the face and tell you i've got fifty thousand pounds. You and I both know, we can't both 
have it
SPEAKER1: [OVERLAP] I've got fifty thousand pounds. And I've got fifty thousand pounds, one thousand five hundred and ten pounds 
SPEAKER2:either you're lying two rounds in a row and you're being truthful or, you've both lied each so I can't trust either of you 
SPEAKER4:at the end of the day Kelly 
SPEAKER1: Kelly in the last round. You know, I went with you. 
SPEAKER2:You did. Yeah, 
SPEAKER1:Because I actually believe
SPEAKER2:And i'm very grateful for that
SPEAKER4:I mean you you were always gonna believe I mean we I believed you had cash Kelly
Obviously, we knew where the killers were so I believed you had cash it was the amounts
But you know having doubted your amounts if there was never any doubt that you had cash
SPEAKER1: [OVERLAP] She's trying to deflect. She's deflecting here.
However, I don't understand Johnny. Johnny. Why did I don't understand why you lied Johnny
SPEAKER1: [OVERLAP] I've got fifty thousand pounds
SPEAKER4:I don't understand why you lied in the first round because you had no reason to inflate the amounts that you had because we knew
Where the killer's were
SPEAKER1: the last round is done though
SPEAKER4: it doesn't, no it's not
SPEAKER1:50,000 pound
SPEAKER2: I'm sorry Johnny I'm going off the last round as well, because that's all i personally have got to go about
SPEAKER4:[OVERLAP] the last round is the most important round to me
SPEAKER1:Don't be such a ridiculous
SPEAKER4:but Johnny that's only your word and I know it's in here so
SPEAKER1: What it is Kelly you've got to either believe me
SPEAKER4:Yeah, 
SPEAKER1:or believe Beverly 
SPEAKER4: and you knew I mean I didn't have a fantastic cash amounts in the first round
But I didn't lie about them because at the end of the day, I don't want to have to lie and I'm not lying I've got the
50,000 pounds here
SPEAKER2: Right
SPEAKER1:Kelly look at her. She's beginning to sweat
SPEAKER4:My heart is, Johnny, my heart's racing I'm sitting with fifty thousand. my heart, is racing
SPEAKER1: [OVERLAP] but your body language. you're body language, you're telling lies
SPEAKER2: Beverly, do you have that fifty thousand pounds?
SPEAKER4: I have fifty thousand pounds.
SPEAKER1: Kelly
SPEAKER2: do you have fifty thousand pounds. 
SPEAKER1: [OVERLAP]i have fifty thousand pounds. I'm telling the truth 
SPEAKER2:my mind's made up. I want someone I can trust in the final
I want someone that's gonna go home and share the money with me. I don't want to go through with liars
I don't want to go through with greedy people. I want to go and I want to split
SPEAKER4: I agree with you
SPEAKER1: [OVERLAP] I want to split, seriously
HOST: Kelly why should they keep you in the game? 
SPEAKER2: I've been honest throughout. I've got a strong front-hand, I haven't lied. 
HOST: Beverly why should they keep you in 
SPEAKER4: because I've played an honest game and I've got this to be honest
It's not even about the fifty thousand. It's about the fact that I'm honest 
HOST: Johnny
SPEAKER1: fifty thousand pound and that's the truth. That is the truth fifty thousand pound
HOST: Right you each have one vote. One of you is not going to be as lucky as you were last time and you'll be leaving the game and taking your golden balls with you
Between the three of you you have to make that decision now
[CUT] The votes are in our players have decided normal service has been resumed the only truth is
Someone is lying and they have fifty thousand reasons for doing so. Who is it? We'll find out after the break [BREAK]
[CUT] Welcome back to Golden Balls before the break the hunt was on for fifty thousand pounds
We know it's out there what we don't know is who's got it. We're about to find out
It's time to reveal who survived and who's been binned
The first vote is for.
Johnny
The second vote is for.
Beverly 
the third and final vote is for.
Johnny.
That must have been a close decision you were claiming ten pounds that there.
It is. that was a new ball I believe.
SPEAKER1: that's correct
HOST: thousand five hundred pounds is that there
SPEAKER1: That is that Jasper.
HOST: now what Kelly wants to know is this or is it not the fifty thousand pound ball? It's four hundred.
Beverly, open the fifty thousand
SPEAKER4: What I said it was all along. 
HOST: and you're claiming
SPEAKER4: The a thousand nine hundred that you had in the first round
HOST: And 
SPEAKER4: the eight hundred fifty that I had
HOST: Well done Kelly obviously telling the truth. 
SPEAKER2: Absolutely.
HOST: 30 pounds. 
SPEAKER2: I think it's this one
HOST: [OVERLAP] that's it. 
SPEAKER2:Yep
HOST: 800 
SPEAKER2:yep
HOST: and of course the 4,000 pounds
There you are. 
Johnny, played a good game there. Obviously you had to claim the 50,000 or you would have been out
We've had great fun. 
SPEAKER1: Yeah. Yeah, enjoyed myself. Thank you. 
HOST: Go back to Akrington
SPEAKER1: [OVERLAP] Akrington 
HOST: with our best wishes, but you need to bin your balls before you go 400 pounds and fifteen hundred, Killer, Gone. good luck
Johnny you've binned your balls and you're out the game. 
SPEAKER1: Thank you very much. Thank you
HOST: [CUT] Well Johnny has gone home to paint his latest inspired masterpiece called Kelly and Beverly after they've come out of the blender.
Ten golden balls remain in play eight cash balls and two killer balls.
They're going back into the ball machine
And of course Amanda the woman with the killer touch has one more final killer ball
But somewhere in those eleven balls
We know are the five cash amounts that can make up your maximum potential win today.
We've seen them already they are,
One thousand nine hundred pounds eight hundred and fifty pounds, eleven thousand pounds
four thousand pounds and that fifty thousand pounds golden ball.
Kelly and Beverly, you've seen off the competition, but now you have to play together to build up as much cash as you can
It's time to play. Bin or win.

[ROUND 3]

HOST: Beverly Kelly you've made it through to the final stages of golden balls congratulations
SPEAKER2: Thank you.
HOST: You had a very difficult decision there. 
SPEAKER2: It was very worrisome 
HOST: but you've got it, right?
So you owe her a favour. All right, but you played a great game to get there
Let's see what happens. I can tell you that your maximum potential jackpot today is
67,750
I'm going to start with you, Beverly, because you brought through the most amount of money.
And first and foremost, you pick a ball to bin.
Whatever you pick, goes out of the game, whether it be that fifty thousand pound ball or a
killer ball.
Now, remember, there are three killers out there.
You then pick a ball to win.
The ball to win goes into this slot here.
And then you pick a ball to bin, you pick a ball to win.
Every ball to win goes into these slots here.
The cash man builds up until, or if, you pick a killer ball.
And then supposing you had ten thousand here and ten thousand there, that's twenty thousand pounds accumulated,
pick a killer ball next, and that twenty thousand pounds goes down to two thousand pounds.
Every killer ball knocks a nought off your total.
That's why you've been trying to avoid them all through the game.
Right, are you ready?
Beverly, pick a ball to bin.
You're in this together, so everybody takes responsibility.
It's not down to one person.
SPEAKER4:What are your thoughts?
SPEAKER2:none at all. Whatever you want to pick, I'm a hundred and ten percent behind you.
SPEAKER4: I don't know why, but because it was the last...
SPEAKER2:I was looking, were you looking at that one, too?
SPEAKER4:Well, it's between that one and that one. And I don't know why, because it was the last ball to go in and I'm minded to go with that one.
This is the one. 
HOST: This is the one to bin. 
SPEAKER4: to bin
HOST: yeah.
You're trying to avoid the fifty thousand-pound ball.
You're trying to pick a killer ball.
SPEAKER2: I'll be happy with that. Good luck. 
HOST: Let's see what you've chosen, Beverly. Oh, not bad. two hundred pounds. 
SPEAKER2: I'm happy with that
HOST: It's one of the lesser balls on the table.
Now, remember, there are three killers out there.
Pick a ball to win.
If it's a killer, it's not too bad at this stage,
because you get rid of one of them and it doesn't affect your cash token.
SPEAKER4: OK, do you want to go with this one?
SPEAKER2: I'll go with that one. 
SPEAKER4: OK.
HOST: Happy? 
SPEAKER4:Yep.
HOST: All right, we're hoping here for a decent cash amount or even a killer.
SPEAKER4: or a killer.
HOST: Or even a killer.
Your first winning golden ball today is...
..a killer.
[UNINTELLIGIBLE]
HOST: You don't have any cash,
but you have got rid of one of the three killer balls,
so there's only two left.
If you can pick the fifty thousand-pound ball into one of these slots,
you're on for some very decent money today.
Kelly, pick a ball to bin.
SPEAKER2:I'm thinking one of these, so... Any thoughts?
SPEAKER4:This is a game of chance, isn't it?
It's totally a game of chance.
Go whichever one you feel. Go with this one.
SPEAKER2:Happy? 
SPEAKER4:Yeah, happy.
HOST: It's a good track record so far. 
SPEAKER4:Yep.
HOST: What have you chosen, Kelly?
SPEAKER2:Woo-hoo!
HOST: One killer left. Good news.
Kelly, pick a ball to win.
Keep up this track record,
and you've got a chance at picking a really good, heavy cash ball.
Come on. 
SPEAKER2:I'm looking at that one there.
Any thoughts? 
SPEAKER4:Go with it. Just go for it.
SPEAKER2:Yeah?
HOST: Again, if it's the killer ball,
it means you've got a bit of all the killers,
but the fifty thousand-pound ball is still out there.
With a bit of luck, it'll be a big cash ball.
Your second-winning golden ball today is. a very nice four thousand pound.
That means you've got four thousand pounds cash.
Your highest possible win is sixty six thousand nine hundred,
providing you can get that fifty thousand-pound ball
that's out there into one of these three slots.
Back to you, Beverly. Pick a ball to bin.
One killer ball left.
Lots of cash.
SPEAKER2:One nearest. 
SPEAKER4:That one or that one?
SPEAKER2:I'd be more inclined to go for that one.
SPEAKER4:I was going to say, go with the one that you've, picked out.
HOST: OK, let's see what you've chosen. Fingers crossed.
SPEAKER4:Come on.
HOST: eight hundred and fifty!
Good one.
So far, so good.
This could be good.
Beverly, pick a ball to win.
Try and get that fifty thousand pound ball if you can.
And, of course, there's an 11,000-pound ball out there.
SPEAKER2:I think it's a bit, somewhere over here.
But not that one or that one.
SPEAKER4:Not that one, that one.But one of those three.
SPEAKER2:Not that one.OK, that one.You pick.I don't know.Let's go for it.
If it's a killer, they're all out the way.
SPEAKER4:Yes.
HOST: Your third winning golden ball today is...
Is this the fifty thousand pound ball?
You'll just have to wait a little bit to find out. [BREAK]
[CUT] Welcome back to Golden Balls.
Before the break, Kelly and Beverly had accumulated
four thousand pounds towards their Golden Five jackpot for today.
What they want to know is this ball the missing killer,
or is it a big cash ball like the fifty thousand pound cash ball
that's still out there or possibly here?
Your third winning golden ball today is...
Well, your luck had to stop at some point.
That is fantastic.
fifty thousand pounds.
It means that your accumulated cash total is fifty thousand and four hundred pounds.
You're on for sixty six thousand pounds.
There's five Golden Balls left,
four of them are cash, including the eleven thousand,
but there is one killer there.
Kelly, pick a ball to bin.
SPEAKER2:I don't like that one over there.
HOST: Is that the one you want? 
SPEAKER2: yeah, let's go for it?
SPEAKER4:Yeah.
HOST: OK, let's see what you chose.
SPEAKER2:Ready?
SPEAKER4:Mhm.
HOST: a thousand nine hundred. That's what you bin.
Pick a ball to win, Kelly.
SPEAKER2: Anything?
SPEAKER4:OK, that's a nice row there, if you leave it.
Let's go for this, then. 
SPEAKER2:All right
SPEAKER4:OK.
HOST: Your fourth winning golden ball today is...
Well, it's a cash-ball.
thirty pounds.
That's fifty four thousand.
You now have thousand and 30 pounds.
Pick a ball to bin, Kelly.
SPEAKER2:Whatever you pick, I'll back you. Ruin this together.Good luck.
HOST: Ooh, the eleven thousand ball.
SPEAKER4:Well...
HOST: Two golden balls left.
One is a killer.
One is worth eight hundred.
Pick the eight hundred pound ball,
and you're going for fifty four thousand eight hundred and thirty pounds.
Pick the killer ball,
and you'll be fighting over five thousand four hundred and three
Pick a ball to win.
SPEAKER4:What did you say about your left side?
SPEAKER2:I don't like it.
SPEAKER4:You don't like your left side.
SPEAKER2:Well, I'm not drawn to that one.
SPEAKER4:Pick.
SPEAKER2:Good luck. I'm happy to do it if you are.
SPEAKER4:Ok, Done. 
SPEAKER2:Good luck.
HOST: Your fifth and final winning ball is.
800 pounds.
Oh, who wants to win it?
Well done. Well played, girls.
That's not bad.
The jackpot today is fifty four thousand eight hundred and thirty pounds.
The golden question is,
can you keep it?
Because it's time to play
split or steal.
Kelly, Beverly,
you both have two golden balls in front of you.
They are the most important balls in the game.
They're not cash and they're not killers.
You each have a golden ball with the word split written inside.
You each have a golden ball with the word steal written inside.
I will ask you to check which is which.
You will both make a conscious decision
to choose either the split or the steal ball.
Neither of you will know what the other has chosen.
If you both choose the split ball,
you'll split today's jackpot at fifty four thousand eight hundred and thirty pounds,
and you'll both go home with twenty seven thousand four hundred and fifteen pounds.
That is serious, serious money.
If one of you chooses split and one of you chooses steal,
whoever chooses the steal ball goes home
with today's jackpot at fifty four thousand eight hundred and thirty pounds,
and whoever chooses the split ball goes home with nothing.
If you both choose the steal ball,
you both go home with nothing.
Do you understand that?
SPEAKER2: Yes.
HOST: Will you please check your two golden balls now
to see which is the split ball and which is the steal?
Don't show each other.
Just make sure you know which is which.
Before I ask you to choose,
I'll just give you the floor for a couple of minutes
to discuss what's happened today.
SPEAKER2:twenty seven thousand pounds is a lot of money to me.
I could do so, so much with that.
SPEAKER4:I would be gutted if I went home with nothing.
SPEAKER2:I'm so scared that you're going to take all of this.
I'm going to split a hundred and ten percent and I'm so scared you're going to take...
SPEAKER4:I have been absolutely honest from start to finish,
and I'm being absolutely honest with you now.
SPEAKER2:You took took a big gamble on me [HARD TO DISTINGUISH DUE TO OVERLAP] and it paid off.
SPEAKER4: And we worked really hard to get this far,
so I would split if you would split.
SPEAKER2: there you go
HOST: Beverly, Kelly, choose split or steal ball now.
Please, and hold it up.
Are you ready?
SPEAKER2: Yeah.
SPEAKER4: Yeah.
HOST: Let's all see it.
SPEAKER2: I'm so sorry.
HOST: Why are you sorry?
SPEAKER2: I do feel bad, but fifty four thousand pounds is a lot of money.
SPEAKER4: You're conscience
HOST: Congratulations, you've stolen fifty four thousand eight hundred and thirty pounds.
Commiseration, Beverly, you go home with nothing.
SPEAKER2: That was the game [UNINTELLIGIBLE].
HOST:It is the game. It is the game. 
All good friendships have to come to an end,
and this one just has.
They've been themselves.
This has been Golden Balls.
Until next time, goodbye.

[END]

SPEAKER4: When Kelly revealed the steal ball,
it's a game of chance at the end of the day.
I didn't know which way it was going to go,
but I'm happy with my decision on the day.
SPEAKER2:I chose the steal ball because I was always going to come here
and steal, I was never going to split.




