# emo_coop_golden_balls

## About this project

This project contains data and code for our paper at Evolang XV:

Baumann, A., Matzinger, T., Mühlenbernd, R., Wacewicz, S., Pleyer, M., Hartmann, S., Placiński, M. (forthc.). The role of linguistically encoded emotional characteristics for cooperativeness in a one-shot prisoner’s dilemma. The Evolution of Language: Proceedings of the 15th International Conference (EVOLANG XV). Preprint: https://phaidra.univie.ac.at/o:2045715

## Setup

Download emotion lexicon from Warriner et al. (2013, Behav. Res. Meth.; https://static-content.springer.com/esm/art%3A10.3758%2Fs13428-012-0314-x/MediaObjects/13428_2012_314_MOESM1_ESM.zip) and save it to `data/` as `warriner_vad.csv`.


Install udipipe model:
```
install.packages("udpipe")
udpipe::udpipe_download_model(language = "english-ewt", model_dir = "models")
```
