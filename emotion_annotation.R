
library(viridis)
library(mgcv)
library(gsheet)
library(readxl)
library(TSclust)


####################### ANNOTATE EMOTIONS ##########################

#### import emo lexicon ####

# memolon dictionary
#memolon = read.delim("data/memolon_en.tsv")

# warriner VAD ratings
warriner = read.csv("data/warriner_vad.csv")
warriner = subset(warriner, select = c(Word, V.Mean.Sum, A.Mean.Sum, D.Mean.Sum))
colnames(warriner) = c("word", "valence", "arousal", "dominance")

# --> emo lexicon needs to be adjusted in import section below (merge())

DICT = warriner
FINAL_ROUND = FALSE
K = 5

#### config ####

dir = "output_updated"


#### general import ####


# get metadata from google table

metadata = read_excel("GoldenBalls_MetaData.xlsx")
metadata = metadata[-1,]


#### process through files ####

results = list()

files = list.files(dir)

for(file in files){
  
  #file = files[1]
  
  data = read.csv(paste0(dir, "/", file))
  
  vocab = data.frame(word = unique(data$lemma))
  emo_dict = merge(DICT, vocab, by = "word")   # dict for episode
  
  episode_num = as.numeric(strsplit(file, "_")[[1]][1])
  
  # get remaining speakers and outcome
  subs = subset(metadata, Episode==episode_num & Choice %in% c("split", "steal"))
  actor_set = paste("speaker", subs$SpeakerNumber, sep="")
  
  outcome = subs$Choice
  gender = subs$Sex 
  
  
  #### restrict to final round ####
  
  if(FINAL_ROUND){
    # only consider final round
    final_idxs = c()
    for(a in setdiff(c("speaker1", "speaker2", "speaker3", "speaker4"), actor_set)){
      final_idx = max(which(data$actor == a))  # final index where speaker "a" shows up
      final_idxs = c(final_idxs, final_idx)
    }
    data = data[(max(final_idxs)+1):nrow(data),]  # restrict data to everything after final index
  }
  
  

  #### compute emo trajectories ####
  
  actors = unique(data$actor)
  all_emo_values = list()
  
    
  
  for(a in actors){
    df = subset(data, actor == a)
    utterances = unique(df$utterance)
    
    emo_names = colnames(emo_dict)[-1]
    emo_values = data.frame(matrix(ncol = length(emo_names), nrow = 0))
    colnames(emo_values) = emo_names
    
    for(utt in utterances){
      utt = subset(df, utterance == utt & is.element(upos, c("PUNCT", "NUM", "SYM")) == FALSE)
      utt = merge(utt, emo_dict, by.x = "lemma", by.y = "word")
      
      utt_emo = t(as.data.frame(colMeans(utt[,c(2,10:ncol(utt))])))
      
      emo_values = rbind(emo_values, utt_emo)
    }
    
    row.names(emo_values) = 1:nrow(emo_values)
    emo_values = na.omit(emo_values)
    all_emo_values[[a]] = emo_values
    
  }
  
  
  #### emotional trajectories of all actors ####
  
  
  emo_vars = c("valence", "arousal", "dominance")
  
  colors = viridis(length(emo_vars))
  names(colors) = emo_vars
  
  
  par(mfrow = c(2,3))
  
  
  for(emo_var in emo_vars){
    
    pvals = c()
    
    ylim = c(1,9)
    if(emo_var=="valence"){ylim = c(4.5,6.5)}
    if(emo_var=="arousal"){ylim = c(3.5,4.5)}
    if(emo_var=="dominance"){ylim = c(4.5,6.5)}
    
    plot(NA, NA, xlim = c(1, max(data$utterance)), ylim = ylim, 
         xlab = "utterance", ylab = emo_var, main = episode_num) 
    
    i = 1
    for(actor in actor_set){
      utt = all_emo_values[[actor]][["utterance"]] 
      emo = all_emo_values[[actor]][[emo_var]] 
      mdl = gam(emo ~ s(utt, k = K))
      lines(utt, emo, col = paste0(substr(colors[[emo_var]], 1, 7), 40), type = "p", pch = i)
      lines(utt, predict(mdl), type = "l", col = colors[[emo_var]], lwd = 4, lty = i)
      #abline(mdl, col = colors[[actor]], lwd = 3)
      i = i+2
      pvals = c(pvals, summary(mdl)$s.pv)
    }
    
    print(emo_var)
    print(data.frame(actor_set, pvals))
  }
  
  
  
  #### emotional alignment of two remaining actors ####
  
  #actor_set_final = c("Speaker 3", "Speaker 4")
  #emo_vars = c("valence", "arousal", "dominance")
  
  #par(mfrow = c(1,3))
  
  ct_values = list()
  ct_error = list()
  slopes = list()
  alignedness = list()
  predictions = list()
  ts_similarity = list()
  
  for(emo_var in emo_vars){
    
    ylim = c(0,0.5)
    
    # actor 1
    actor = actor_set[1]
    utt = all_emo_values[[actor]][["utterance"]] 
    emo = all_emo_values[[actor]][[emo_var]] 
    mdl1 = gam(emo ~ s(utt, k = K))
    
    # actor 2
    actor = actor_set[2]
    utt = all_emo_values[[actor]][["utterance"]] 
    emo = all_emo_values[[actor]][[emo_var]] 
    mdl2 = gam(emo ~ s(utt, k = K))
    
    
    range = 1:max(data$utterance)
    
    pred1 = predict(mdl1, newdata = data.frame(utt = range))
    pred2 = predict(mdl2, newdata = data.frame(utt = range))
    
    diff = abs(pred1 - pred2)
    plot(range, diff, xlab = "utterance", ylab = paste("delta", emo_var), ylim = ylim, col = "#80808040") 
    
    mdl = lm(diff ~ range)
    smry = summary(mdl)
    pval = smry$coefficients[2,4]
    change = smry$coefficients[2,1] * max(data$utterance)  # total change 
    
    abline(mdl, col = "black", lty = 2, lwd = 2)
    
    ct = cor.test(range, diff, method = "pearson")
    
    if(pval < 0.05){star = "*"} else {star = ""}
    text(floor(max(data$utterance)/2), max(ylim-0.1), labels = paste0(round(change, 2), star), cex = 2)
    
    print(emo_var)
    print(ct)
    
    ct_values[[emo_var]] = ct$estimate
    ct_error[[emo_var]] = abs(diff(ct$conf.int))
    slopes[[emo_var]] = change
    alignedness[[emo_var]] = mean(as.numeric(predict(mdl))) # this is just the first value of the model
    predictions[[emo_var]] = list(pred1, pred2)
    ts_similarity[[emo_var]] = diss.DTWARP(pred1, pred2)   # cor(pred1, pred2)
  }
  
  
  # add results
  results[[episode_num]] = list(ct_values, ct_error, slopes, alignedness, predictions, outcome, gender, ts_similarity)
}

saveRDS(results, "results.rds")






