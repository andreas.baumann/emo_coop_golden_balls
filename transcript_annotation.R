############## ANNOTATE TRANSCRIPTS WITH UDPIPE ##############

# installation: 
# install.packages("udpipe")

# download English model:
# udpipe::udpipe_download_model(language = "english-ewt", model_dir = "models")

# for training your own models see:
# vignette("udpipe-train", package = "udpipe")

# nice tutorial:
# https://ladal.edu.au/tagging.html


library(udpipe)
library(progress)


#### config ####

# directories
indir = "transcripts"
outdir = "transcripts_updated"

# load model
mdl = udpipe_load_model(file = "models/english-ewt-ud-2.5-191206.udpipe")



#### helpers ####

annotate_transcript = function(indir, file, outdir, mdl){
  # indir ... directory with input files
  # file .... file name of txt
  # outdir .. directory for output
  # mdl ..... udpipe model
  
  # get texts
  lines = readLines(paste(indir, file, sep = "/"))   
  lines = lines[grepl(":", lines, fixed = TRUE)]     # only lines with ":" in it
  
  # define data frame for parsed data
  parsed_lines = data.frame(
    utterance = numeric(),
    actor = character(),
    sentence_id = integer(),
    sentence = character(),
    token_id = character(),
    lemma = character(),
    upos = character(),
    xpos = character()
  )
  
  
  for(i in 1:length(lines)){
    # get actor and text
    split = strsplit(lines[i], ":")[[1]]
    actor = gsub(" ", "", tolower(split[1]))
    text = paste(split[2:length(split)], sep = ":")   # merge the text together again
    text = tolower(text)                              # lowercase text
    
    # parse text 
    df = udpipe_annotate(mdl, x = text)
    df = as.data.frame(df)[,3:9]     # drop doc_id, paragraph_id, and syntactic dependency info
    
    # combine parsed text with metadata 
    utterance = rep(i, nrow(df))
    actor = rep(actor, nrow(df))
    
    df = cbind(
      utterance,
      actor,
      df
    )
    
    # append to parsed lines
    parsed_lines = rbind(parsed_lines, df)
    
  }
  
  # write to output directory
  write.csv(
    parsed_lines, 
    file = paste(outdir, "/", gsub(".txt", "", file), "_annotated.csv", sep = ""), 
    row.names = FALSE
    )
}



#### annotate all transcripts ####

files = list.files(indir)
pb = progress_bar$new(total = length(files))

loop = function(){
  pb$tick(0)
  
  for(file in files){
    pb$tick()
    annotate_transcript(indir = indir, file = file, outdir = outdir, mdl)
  }
}
loop()





